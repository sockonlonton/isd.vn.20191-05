REVIEW GROUP 05
* Huyền review Hùng:
- Về sequence diagram: 
     + Usecase " Enter by card" 
	  1.Bước yêu cầu mở cổng lẽ ra cần thực hiện trước bước " hiển thị thông tin thẻ" và " hiển thị thẻ không hợp lệ".
	  2.Các hành động phải bắt đầu bằng động từ.
	  3.Nhiều bước không có return message.
     + Usecase " Exit by card"
	  1. Nhiều bước không có return message.
- Về analysis diagram:
     + Chưa phân biệt public và private.
* Hùng reviews Hưng:
 - Hiểu bài và làm bài đầy đủ.
 - Phần usecase Exit by one-way ticket làm rất chi tiết.
 - Phần usecase Exit by 24h ticket làm còn sơ sài.
* Hưng reviews Huyền:
- Trong analysis class diagram chưa phân biệt được public và private.
- Sao lại cho cả gate vào làm class?

