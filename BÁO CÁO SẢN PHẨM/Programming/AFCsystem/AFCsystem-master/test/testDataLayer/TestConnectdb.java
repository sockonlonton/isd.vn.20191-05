package testDataLayer;

import static org.junit.Assert.*;

import java.sql.Connection;
import java.sql.SQLException;

import org.junit.Test;

import dataLayer.Connectdb;

public class TestConnectdb extends Connectdb{

	@Test
	public void testGetPostgresqlConnection() throws ClassNotFoundException, SQLException {
		System.out.println("testGetPostgresqlConnection");
		Connection conn = getPostgresqlConnection();
		System.out.println("Get connection " + conn);
	    System.out.println("Done!");
	}

}
