package testDataLayer;

import static org.junit.Assert.*;

import java.util.Scanner;

import org.junit.Test;

import dataLayer.ReadInfoText;

public class TestReadInfoText {
	ReadInfoText re = new ReadInfoText();
	@Test
	public void testDirectoryCurrent() {
		System.out.println("");
		System.out.println(re.directoryCurrent());
	}

	@Test
	public void testReadFile() {
		System.out.println("testReadFile");
		Scanner sc = new Scanner(System.in);
		System.out.println("file name :");
		String filename = sc.nextLine();
		String[] text = re.readFile(filename);
		for(int i=0;i<text.length;i++) {
			if(text[i] != null) {
				System.out.println(text[i]);
			}
		}
	}	

	@Test
	public void testEditFile() {
		System.out.println("testEditFile");
		fail("Not yet implemented");
	}

}
