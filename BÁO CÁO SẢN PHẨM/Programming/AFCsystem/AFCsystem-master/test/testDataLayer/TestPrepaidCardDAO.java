package testDataLayer;

import static org.junit.Assert.*;

import java.util.ArrayList;

import org.junit.Test;

import dataLayer.PrepaidCardDAO;
import dataObject.PrepaidCardDTO;

public class TestPrepaidCardDAO {

	@Test
	public void testSelectCard() {
		System.out.println("testSelectCard");
		PrepaidCardDAO pr = new PrepaidCardDAO();
		ArrayList<PrepaidCardDTO> allCard = new ArrayList<PrepaidCardDTO>();
		allCard = pr.selectCard();
		for(PrepaidCardDTO card : allCard) {
			System.out.println(card.getId()+"-"+card.getBalance());
		}
	}

}
