/*
 *System Name: AFCsystem 
 */
package testBussinessLayer;

import static org.junit.Assert.*;

import org.junit.Test;

import bussinessLayer.Ticket24hBUS;
import dataObject.Ticket24hDTO;

// TODO: Auto-generated Javadoc
/**
 * The Class testTicket24hBUS.
 */
public class TestTicket24hBUS {
	
	/** The ti. */
	private Ticket24hBUS ti = new Ticket24hBUS();
	
	/**
	 * Test load list ticket 24 h.
	 */
	@Test
	public void testLoadListTicket24h() {
		System.out.println("testLoadListTicket24h");
		for(Ticket24hDTO tf : ti.loadListTicket24h()) {
			System.out.println(tf.getPseudocode()+"-"+tf.getStatus());
		}
	}

	/**
	 * Test format date.
	 */
	@Test
	public void testFormatDate() {
		System.out.println("testFormatDate");
		System.out.println(ti.formatDate());
	}
}
