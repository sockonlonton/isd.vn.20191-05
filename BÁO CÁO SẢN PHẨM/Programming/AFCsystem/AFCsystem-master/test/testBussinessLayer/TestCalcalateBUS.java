/*
 *System Name: AFCsystem 
 */
package testBussinessLayer;

import static org.junit.Assert.*;

import java.util.Scanner;

import org.junit.Test;

import bussinessLayer.CalculateBUS;

// TODO: Auto-generated Javadoc
/**
 * The Class testCalcalateBUS.
 */
public class TestCalcalateBUS {
	
	/** The ca. */
	private CalculateBUS ca = new CalculateBUS();
	
	/**
	 * Test distance.
	 */
	@Test
	public void testDistance() {
		System.out.println("Test Distance");
		Scanner sc = new Scanner(System.in);
		System.out.print("IdStart :");
		char idStart = sc.nextLine().charAt(0);
		System.out.print("IdEnd :");
		char idEnd = sc.nextLine().charAt(0);
		float distance = ca.distance(idStart, idEnd);
		System.out.println("distance :" +distance);
	}

	/**
	 * Test pricing.
	 */
	@Test
	public void testPricing() {
		System.out.println("Test Pricing");
		System.out.print("distance :");
		Scanner sc = new Scanner(System.in);
		float distance = sc.nextFloat();
		System.out.println("price: "+ca.pricing(distance));
	}

	/**
	 * Test check balance.
	 */
	@Test
	public void testCheckBalance() {
		Scanner sc = new Scanner(System.in);
		System.out.println("Test checkBalance");
		System.out.print("balance :");
		float balance = sc.nextFloat();
		System.out.print("price: ");
		float price = sc.nextFloat();
		System.out.println(ca.checkBalance(balance, price));
	}

}
