/*
 *System Name: AFCsystem 
 */
package testBussinessLayer;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.Scanner;

import org.junit.Test;

import bussinessLayer.CardBUS;
import bussinessLayer.PrepaidCardBUS;
import dataObject.PrepaidCardDTO;

// TODO: Auto-generated Javadoc
/**
 * The Class testPrepaidCardBUS.
 */
public class TestPrepaidCardBUS {
	
	/** The pr. */
	private PrepaidCardBUS pr = new PrepaidCardBUS();

	/**
	 * Test load list prepaid card.
	 */
	@Test
	public void testLoadListPrepaidCard() {
		System.out.println("testLoadListPrepaidCard");
		ArrayList<PrepaidCardDTO> allCard = new ArrayList<PrepaidCardDTO>();
		allCard = pr.loadListPrepaidCard();
		for(PrepaidCardDTO card : allCard) {
			System.out.println(card.getPseudocode()+"-"+card.getBalance());
		}
	}


	/**
	 * Test isset prepaid card.
	 */
	@Test
	public void testIssetPrepaidCard() {
		System.out.println("testIssetPrepaidCard");
		Scanner sc = new Scanner(System.in);
		System.out.print("psedocode :");
		String cardCodePseudo = sc.nextLine();
		System.out.println(pr.issetPrepaidCard(cardCodePseudo));
	}

	/**
	 * Test display prepaid card error.
	 */
	@Test
	public void testDisplayPrepaidCardError() {
		System.out.println("testDisplayPrepaidCardError");
		PrepaidCardDTO card = new PrepaidCardDTO();
		card.setId("201912060000");
		card.setBalance((float)0.5);
		String error = "Card not entered.";
		pr.displayPrepaidCardError(card, error);
	}

	/**
	 * Test display prepaidcard error.
	 */
	@Test
	public void testDisplayPrepaidcardError() {
		System.out.println("testDisplayPrepaidcardError");
		PrepaidCardDTO card = new PrepaidCardDTO();
		card.setId("201912060000");
		card.setBalance((float)0.5);
		String error = "Not enough balance: ";
		float price = (float) 3;
		pr.displayPrepaidcardError(card, error, price);
	}
}
