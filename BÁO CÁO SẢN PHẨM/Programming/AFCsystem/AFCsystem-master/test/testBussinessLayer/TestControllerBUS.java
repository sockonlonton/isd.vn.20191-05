package testBussinessLayer;

import static org.junit.Assert.*;

import java.sql.SQLException;
import java.util.Scanner;

import org.junit.Test;

import bussinessLayer.ControllerBUS;

public class TestControllerBUS {
	ControllerBUS c = new ControllerBUS();
	@Test
	public void testCheckInput() {
		Scanner sc = new Scanner(System.in);
	    String choice = sc.nextLine();
	    System.out.println(c.checkInput(choice));
	}

	@Test
	public void testGetDataInput() throws ClassNotFoundException, SQLException {
		Scanner sc = new Scanner(System.in);
	    String choice = sc.nextLine();
	    ControllerBUS c = new ControllerBUS();
	    c.GetDataInput(choice);
	}
}
