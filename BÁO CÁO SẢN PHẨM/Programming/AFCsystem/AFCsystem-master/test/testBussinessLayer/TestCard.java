/*
 *System Name: AFCsystem 
 */
package testBussinessLayer;

import static org.junit.Assert.*;

import java.util.Scanner;

import org.junit.Test;

import bussinessLayer.CardBUS;

// TODO: Auto-generated Javadoc
/**
 * The Class testCard.
 */
public class TestCard extends CardBUS{
	
	/** The ca. */
	CardBUS ca = new CardBUS();
	
	/**
	 * Test create id card.
	 */
	@Test
	public void testCreateIdCard() {
		System.out.println("testCreateIdCard");
		String id = ca.createIdCard();
		System.out.println(id);
	}

	/**
	 * Test create card corres code.
	 */
	@Test
	public void testCreateCardCorresCode() {
		System.out.println("testCreateCardCorresCode");
		System.out.println("pseudoCode :");
		Scanner sc = new Scanner(System.in);
		String cardPseudoCode = sc.nextLine();
		String corresCode = ca.createCardCorresCode(cardPseudoCode);
		System.out.println(corresCode);
	}

	/**
	 * Test controll open gate.
	 */
	@Test
	public void testControllOpenGate() {
		System.out.println("testControllOpenGate");
		ca.controllOpenGate();
	}

}
