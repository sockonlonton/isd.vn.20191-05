/*
 *System Name: AFCsystem 
 */
package testBussinessLayer;

import static org.junit.Assert.*;

import java.util.Scanner;

import org.junit.Test;

import bussinessLayer.TicketOnewayBUS;
import dataObject.Ticket24hDTO;
import dataObject.TicketOnewayDTO;

// TODO: Auto-generated Javadoc
/**
 * The Class testTicketOnewayBUS.
 */
public class TestTicketOnewayBUS {
	
	/** The ti. */
	TicketOnewayBUS ti = new TicketOnewayBUS();
	
	/**
	 * Test load list ticket oneway.
	 */
	@Test
	public void testLoadListTicketOneway() {
		System.out.println("testLoadListTicketOneway");
		for(TicketOnewayDTO ow : ti.loadListTicketOneway()) {
			System.out.println(ow.getPseudocode()+":"+ow.getNameStart()+"-"+ow.getNameEnd()+"-"+ow.getBalance());
		}
	}


	/**
	 * Test check station.
	 */
	@Test
	public void testCheckStation() {
		System.out.println("testCheckStation");
		TicketOnewayDTO ow = new TicketOnewayDTO();
		Scanner sc = new Scanner(System.in);
		System.out.print("id station: ");
		char idStation = sc.nextLine().charAt(0);
		System.out.print("idStart: ");
		ow.setIdStart(sc.nextLine().charAt(0));
		System.out.print("idEnd: ");
		ow.setIdEnd(sc.nextLine().charAt(0));
		System.out.println(ti.checkStation(ow, idStation));
	}
}
