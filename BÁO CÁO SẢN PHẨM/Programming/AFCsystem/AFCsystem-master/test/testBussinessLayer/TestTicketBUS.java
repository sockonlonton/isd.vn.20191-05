/*
 *System Name: AFCsystem 
 */
package testBussinessLayer;

import static org.junit.Assert.*;

import java.util.Scanner;

import org.junit.Test;

import bussinessLayer.TicketBUS;

// TODO: Auto-generated Javadoc
/**
 * The Class testTicketBUS.
 */
public class TestTicketBUS {
	
	/** The ca. */
	private TicketBUS ca = new TicketBUS();
	
	/**
	 * Test create id ticket.
	 */
	@Test
	public void testCreateIdTicket() {
		System.out.println("testCreateIdTicket");
		String id = ca.createIdTicket();
		System.out.println(id);
	}

	/**
	 * Test create ticket corres code.
	 */
	@Test
	public void testCreateTicketCorresCode() {
		System.out.println("testCreateTicketCorresCode");
		System.out.println("pseudoCode :");
		Scanner sc = new Scanner(System.in);
		String ticketPseudocode = sc.nextLine();
		String corresCode = ca.createTicketCorresCode(ticketPseudocode);
		System.out.println(corresCode);
	}

	/**
	 * Test controll open gate.
	 */
	@Test
	public void testControllOpenGate() {
		System.out.println("testControllOpenGate");
		ca.controllOpenGate();
	}

}
