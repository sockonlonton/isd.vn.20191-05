/*
 * 
 */
package dataLayer;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

// TODO: Auto-generated Javadoc
/**
 * The Class connectdb.
 */
public class Connectdb {
	
	/**
	 * Gets the postgresql connection.
	 *
	 * @return the postgresql connection
	 * @throws SQLException the SQL exception
	 * @throws ClassNotFoundException the class not found exception
	 */
	public static Connection getPostgresqlConnection() throws SQLException,ClassNotFoundException {
		String url = "jdbc:postgresql://localhost/AFCsystem";
	    String user = "postgres";
	    String password = "nguyenhung6256";
	    Class.forName("org.postgresql.Driver");
		Connection conn = DriverManager.getConnection(url,user,password);
		return conn;
	}
}
