/*
 * 
 */
package dataLayer;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.util.ArrayList;

import dataObject.Ticket24hDTO;

// TODO: Auto-generated Javadoc
/**
 * The Class readInfoText.
 */
public class ReadInfoText {
	
	/**
	 * Directory current.
	 *
	 * @return the string
	 */
	public String directoryCurrent() {
		File file = new File("");
        String currentDirectory = file.getAbsolutePath();
        currentDirectory = currentDirectory+"/info";
		return currentDirectory;
	}
	
	/**
	 * Read file.
	 *
	 * @param filename the filename
	 * @return the string[]
	 */
	public String[] readFile(String filename) {
		BufferedReader br = null;		
        String[] textInALine = new String[5];
        try {   
        	int i=0;
        	br = new BufferedReader(new FileReader(directoryCurrent()+"/"+filename));
            while ((textInALine[i] = br.readLine()) != null) {
            	i++;
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                br.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return textInALine;
	}
	
	/**
	 * Edits the file.
	 *
	 * @param textInAline the text in aline
	 * @param filename the filename
	 */
	public void editFile(String[] textInAline,String filename) {
		try {
			FileWriter writer = new FileWriter(directoryCurrent()+"/"+filename);
			BufferedWriter buffer = new BufferedWriter(writer);
			for(int i=0;i<textInAline.length;i++) {
				if(textInAline[i] != null) {
					buffer.write(textInAline[i]+"\n");
				}
			}
			buffer.close();
		}
		catch (Exception ex) {
			System.out.println("Loi ghi file: "+ex);
		}
	}
}
