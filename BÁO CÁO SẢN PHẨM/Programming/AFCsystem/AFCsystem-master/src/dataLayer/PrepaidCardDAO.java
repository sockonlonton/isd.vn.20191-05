/*
 * 
 */
package dataLayer;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;

import dataObject.*;

// TODO: Auto-generated Javadoc
/**
 * The Class prepaidCardDAO.
 */
public class PrepaidCardDAO extends Connectdb{
    
    /** The query. */
    private String query;
    
    /**
     * Select card.
     *
     * @return the array list
     */
    public ArrayList<PrepaidCardDTO> selectCard() {
    	ArrayList<PrepaidCardDTO> allCard = new ArrayList<PrepaidCardDTO>();
	    try {
			Connection conn = getPostgresqlConnection();
			query = "select * from prepaidcard"; 
			PreparedStatement sml = conn.prepareStatement(query);
			ResultSet Rs = sml.executeQuery();
			while(Rs.next()) {
				PrepaidCardDTO card = new PrepaidCardDTO();
				card.setId(Rs.getString(1));
				card.setPseudocode(Rs.getString(2));
				card.setCorrescode(Rs.getString(3));
				card.setBalance(Rs.getFloat(4));
				card.setStatus(Rs.getInt(5));
				allCard.add(card);
			}
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
	    return allCard;
    }
    
    /**
     * Insert card.
     *
     * @param card the card
     */
    public void insertCard(PrepaidCardDTO card) {
    	try {
			Connection conn = getPostgresqlConnection();
			Statement st = conn.createStatement();
			query = "INSERT INTO prepaidcard values ('" + card.getId() +"','"+card.getPseudocode()+"','"+card.getCorrescode()+"','"+card.getBalance() + "','1')";
			st.executeUpdate(query);
			
		} catch (Exception e) {
			System.err.println(e.getMessage());
		}
    }
    
    /**
     * Update card.
     *
     * @param card the card
     */
    public void updateCard(PrepaidCardDTO card) {
    	try {
			Connection conn = getPostgresqlConnection();
			Statement st = conn.createStatement();
			query = "UPDATE prepaidcard SET cardbalance = " + card.getBalance() + ",cardstatus=0 WHERE cardpseudocode='"+card.getPseudocode()+"'";
			st.executeUpdate(query);
			
		} catch (Exception e) {
			System.err.println(e.getMessage());
		}
    }
    
    /**
     * Update status.
     *
     * @param card the card
     */
    public void updateStatus(PrepaidCardDTO card) {
    	try {
			Connection conn = getPostgresqlConnection();
			Statement st = conn.createStatement();
			query = "UPDATE prepaidcard SET cardstatus=1 WHERE cardpseudocode='"+card.getPseudocode()+"'";
			st.executeUpdate(query);
			
		} catch (Exception e) {
			System.err.println(e.getMessage());
		}
    }
}
