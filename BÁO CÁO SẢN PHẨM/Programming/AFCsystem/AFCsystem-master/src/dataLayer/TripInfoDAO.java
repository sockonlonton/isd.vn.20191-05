/*
 * 
 */
package dataLayer;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import dataObject.PrepaidCardDTO;
import dataObject.Ticket24hDTO;
import dataObject.TicketOnewayDTO;


// TODO: Auto-generated Javadoc
/**
 * The Class tripInfoDAO.
 */
public class TripInfoDAO extends Connectdb{
    
    /** The query. */
    private String query;
	
	/**
	 * Insert start station.
	 *
	 * @param idStation the id station
	 */
	public void insertStartStation(char idStation){
		try {
			Connection conn = getPostgresqlConnection();
			Statement st = conn.createStatement();
			query = "INSERT INTO tripinfo (idstartstation) values ('" + idStation + "')";
			st.executeUpdate(query);
			
		} catch (Exception e) {
			System.err.println(e.getMessage());
		}
	}
	
	/**
	 * Update code.
	 *
	 * @param pserdocode the pserdocode
	 */
	public void updateCode(String pserdocode) {
		int idTrip = 0;
		try {
			Connection conn = getPostgresqlConnection();
			query = "select max(id) from tripinfo"; 
			PreparedStatement sml = conn.prepareStatement(query);
			ResultSet Rs = sml.executeQuery();
			while(Rs.next()) {
				idTrip = Rs.getInt(1);
			}
		}
		catch(Exception e) {
			System.err.println(e.getMessage());
		}
		try {
			Connection conn = getPostgresqlConnection();
			Statement st = conn.createStatement();
			query = "UPDATE tripinfo SET pseudocode = '"+pserdocode+"' WHERE id = "+idTrip;
			st.executeUpdate(query);
			
		} catch (Exception e) {
			System.err.println(e.getMessage());
		}
	}
	
	/**
	 * Search end station.
	 *
	 * @param tf the tf
	 * @return the char
	 */
	public char searchEndStation(Ticket24hDTO tf) {
		char idEndStation = 0;
		try {
			Connection conn = getPostgresqlConnection();
			query = "select idendstation from tripinfo  group by id having id = All(select max(id) from tripinfo where pseudocode = '"+ tf.getPseudocode() +"')"; 
			PreparedStatement sml = conn.prepareStatement(query);
			ResultSet Rs = sml.executeQuery();
			while(Rs.next()) {
				if(Rs.getString(1) != null) {
					idEndStation = Rs.getString(1).charAt(0);
				}
			}
		}
		catch (Exception e) {
			System.err.println(e.getMessage());
		}
		return idEndStation;
	}
	
	/**
	 * Search start station.
	 *
	 * @param card the card
	 * @param idEndStation the id end station
	 * @return the char
	 */
	public char searchStartStation(PrepaidCardDTO card, char idEndStation) {
		char idStartStation = 0;
		int maxId = 0;
		try {
			Connection conn = getPostgresqlConnection();
			query = "select idstartstation,max(id) from tripinfo  group by id having id = All(select max(id) from tripinfo where pseudocode = '"+ card.getPseudocode() +"')"; 
			PreparedStatement sml = conn.prepareStatement(query);
			ResultSet Rs = sml.executeQuery();
			while(Rs.next()) {
				idStartStation = Rs.getString(1).charAt(0);
				maxId = Rs.getInt(2);
			}
		}
		catch (Exception e) {
			System.err.println(e.getMessage());
		}
		try {
			Connection conn = getPostgresqlConnection();
			Statement st = conn.createStatement();
			query = "UPDATE tripinfo SET idendstation = '" 
					+ idEndStation 
					+ "' WHERE pseudocode = '"
					+ card.getPseudocode()
					+ "'AND id = " + maxId;
			st.executeUpdate(query);
		} catch (Exception e) {
			System.err.println(e.getMessage());
		}
		return idStartStation;
	}
	
	/**
	 * Update end station.
	 *
	 * @param tf the tf
	 * @param idEndStation the id end station
	 */
	public void updateEndStation(Ticket24hDTO tf, char idEndStation) {
		char idStartStation = 0;
		int maxId = 0;
		try {
			Connection conn = getPostgresqlConnection();
			query = "select idstartstation,max(id) from tripinfo  group by id having id = All(select max(id) from tripinfo where pseudocode = '"+ tf.getPseudocode() +"')"; 
			PreparedStatement sml = conn.prepareStatement(query);
			ResultSet Rs = sml.executeQuery();
			while(Rs.next()) {
				idStartStation = Rs.getString(1).charAt(0);
				maxId = Rs.getInt(2);
			}
		}
		catch (Exception e) {
			System.err.println(e.getMessage());
		}
		try {
			Connection conn = getPostgresqlConnection();
			Statement st = conn.createStatement();
			query = "UPDATE tripinfo SET idendstation = '" 
					+ idEndStation 
					+ "' WHERE pseudocode = '"
					+ tf.getPseudocode()
					+ "'AND id = " + maxId;
			st.executeUpdate(query);
		} catch (Exception e) {
			System.err.println(e.getMessage());
		}
	}
	
	/**
	 * Search start station.
	 *
	 * @param ow the ow
	 * @param idEndStation the id end station
	 * @return the char
	 */
	public char searchStartStation(TicketOnewayDTO ow, char idEndStation) {
		char idStartStation = 0;
		int maxId = 0;
		try {
			Connection conn = getPostgresqlConnection();
			query = "select idstartstation,max(id) from tripinfo  group by id having id = All(select max(id) from tripinfo where pseudocode = '"+ ow.getPseudocode() +"')"; 
			PreparedStatement sml = conn.prepareStatement(query);
			ResultSet Rs = sml.executeQuery();
			while(Rs.next()) {
				idStartStation = Rs.getString(1).charAt(0);
				maxId = Rs.getInt(2);
			}
		}
		catch (Exception e) {
			System.err.println(e.getMessage());
		}
		try {
			Connection conn = getPostgresqlConnection();
			Statement st = conn.createStatement();
			query = "UPDATE tripinfo SET idendstation = '" 
					+ idEndStation 
					+ "' WHERE pseudocode = '"
					+ ow.getPseudocode()
					+ "'AND id = " + maxId;
			st.executeUpdate(query);
		} catch (Exception e) {
			System.err.println(e.getMessage());
		}
		return idStartStation;
	}
}
