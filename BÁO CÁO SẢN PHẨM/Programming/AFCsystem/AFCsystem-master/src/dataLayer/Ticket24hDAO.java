/*
 * 
 */
package dataLayer;

import java.sql.Connection;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import dataObject.Ticket24hDTO;
import dataObject.TicketOnewayDTO;

// TODO: Auto-generated Javadoc
/**
 * The Class ticket24hDAO.
 */
public class Ticket24hDAO extends Connectdb{
	
	/** The query. */
	private String query;
	
	/**
	 * Insert ticket 24 h.
	 *
	 * @param tf the tf
	 */
	public void insertTicket24h(Ticket24hDTO tf) {
		Calendar c = Calendar.getInstance();
		c.roll(Calendar.DAY_OF_YEAR, true);
		SimpleDateFormat dateFormat1 = new SimpleDateFormat("yyyy-MM-dd");
		SimpleDateFormat dateFormat2 = new SimpleDateFormat("HH:mm:ss");
		try {
			Connection conn = getPostgresqlConnection();
			Statement st = conn.createStatement();
			query = "INSERT INTO twentyfourhoursticket VALUES ('"+tf.getId()+"','"+tf.getPseudocode()+"','"+
					tf.getCorrescode()+"','"+tf.getStatus()+"','"+dateFormat1.format(c.getTime())+"','"+
					dateFormat2.format(c.getTime())+"')";
			st.executeUpdate(query);
		}
		catch (Exception e) {
			System.err.println(e.getMessage());
		}
	}
}
