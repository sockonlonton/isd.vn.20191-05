/*
 * 
 */
package dataLayer;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

// TODO: Auto-generated Javadoc
/**
 * The Class stationDAO.
 */
public class StationDAO extends Connectdb {
    
    /** The query. */
    private String query;
    
    /**
     * Distance.
     *
     * @param idStation the id station
     * @return the float
     */
    public float distance(char idStation) {
    	float distance=0;
    	try {
			Connection conn = getPostgresqlConnection();
			query = "select distance from station where stationid = '"+idStation +"'"; 
			PreparedStatement sml = conn.prepareStatement(query);
			ResultSet Rs = sml.executeQuery();
			while(Rs.next()) {
				distance = Rs.getFloat(1);
			}
		}
		catch(Exception e) {
			System.err.println(e.getMessage());
		}
    	return distance;
    }
}
