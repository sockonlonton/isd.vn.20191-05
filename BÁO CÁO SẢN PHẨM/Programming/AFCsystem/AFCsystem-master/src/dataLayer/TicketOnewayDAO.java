/*
 * 
 */
package dataLayer;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;

import dataObject.PrepaidCardDTO;
import dataObject.TicketOnewayDTO;

// TODO: Auto-generated Javadoc
/**
 * The Class ticketOnewayDAO.
 */
public class TicketOnewayDAO extends Connectdb{
	
	/** The query. */
	private String query;
	
	/**
	 * Search id station.
	 *
	 * @param nameStation the name station
	 * @return the char
	 */
	public char searchIdStation(String nameStation) {
		char idStation = 0;
		try {
				Connection conn = getPostgresqlConnection();
				query = "select stationid from station where stationname = '"+nameStation+"'"; 
				PreparedStatement sml = conn.prepareStatement(query);
				ResultSet Rs = sml.executeQuery();
				while(Rs.next()) {
					idStation = Rs.getString(1).charAt(0);
				}
		}
		catch (Exception e) {
			System.err.println(e.getMessage());
		}
		return idStation;
	}
	
	/**
	 * Insert oneway ticket.
	 *
	 * @param ow the ow
	 */
	public void insertOnewayTicket(TicketOnewayDTO ow) {
		try {
			Connection conn = getPostgresqlConnection();
			Statement st = conn.createStatement();
			query = "INSERT INTO onewayticket VALUES ('"+ow.getId()+"','"+ow.getPseudocode()+"','"
			+ow.getCorrescode()+"','"+ow.getBalance()+"','"+ow.getStatus()+"')";
			st.executeUpdate(query);
		}
		catch (Exception e) {
			System.err.println(e.getMessage());
		}
	}
	 
 	/**
 	 * Update balance.
 	 *
 	 * @param ow the ow
 	 */
 	public void updateBalance(TicketOnewayDTO ow) {
	    	try {
				Connection conn = getPostgresqlConnection();
				Statement st = conn.createStatement();
				query = "UPDATE onewayticket SET owbalance = " + ow.getBalance() + ",owstatus = '" + ow.getStatus()
						+ "' WHERE owpseudocode='"+ow.getPseudocode()+"'";
				st.executeUpdate(query);
				
			} catch (Exception e) {
				System.err.println(e.getMessage());
			}
	    }
}
