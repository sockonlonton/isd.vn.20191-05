/*
 * 
 */
package bussinessLayer;

import java.util.ArrayList;

import dataObject.Ticket24hDTO;

// TODO: Auto-generated Javadoc
/**
 * The Interface ticket24hInterface.
 */
public interface Ticket24hInterface {
	
	/**
	 * Load list ticket 24 h.
	 *
	 * @return the array list
	 */
	public ArrayList<Ticket24hDTO> loadListTicket24h();
	
	/**
	 * Format date.
	 *
	 * @return the string
	 */
	public String formatDate();
	
	/**
	 * Format output file.
	 *
	 * @param allTicket24h the all ticket 24 h
	 * @return the string[]
	 */
	public String[] formatOutputFile(ArrayList<Ticket24hDTO> allTicket24h);
	
	/**
	 * Enter by ticket 24 h new.
	 *
	 * @param tf the tf
	 */
	public void enterByTicket24hNew(Ticket24hDTO tf);
	
	/**
	 * Check valid until ticket 24 h.
	 *
	 * @param tf the tf
	 * @return true, if successful
	 */
	public boolean checkValidUntilTicket24h(Ticket24hDTO tf);
	
	/**
	 * Check ticket 24 h using.
	 *
	 * @param tf the tf
	 * @return true, if successful
	 */
	public boolean checkTicket24hUsing(Ticket24hDTO tf);
	
	/**
	 * Choose ticket 24 h.
	 *
	 * @param tf the tf
	 * @param action the action
	 * @param idStation the id station
	 */
	public void chooseTicket24h(Ticket24hDTO tf, char action, char idStation);
}
