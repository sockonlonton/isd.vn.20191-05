/*
 * 
 */
package bussinessLayer;

import java.util.ArrayList;

import dataObject.TicketOnewayDTO;

// TODO: Auto-generated Javadoc
/**
 * The Interface ticketOnewayInterface.
 */
public interface TicketOnewayInterface {
	
	/**
	 * Load list ticket oneway.
	 *
	 * @return the array list
	 */
	public ArrayList<TicketOnewayDTO> loadListTicketOneway();
	
	/**
	 * Format output file.
	 *
	 * @param allTicketOneway the all ticket oneway
	 * @return the string[]
	 */
	public String[] formatOutputFile(ArrayList<TicketOnewayDTO> allTicketOneway);
	
	/**
	 * Search id station.
	 *
	 * @param ow the ow
	 * @return the ticket oneway DTO
	 */
	public TicketOnewayDTO searchIdStation(TicketOnewayDTO ow);
	
	/**
	 * Enter by ticket oneway.
	 *
	 * @param ow the ow
	 */
	public void enterByTicketOneway(TicketOnewayDTO ow);
	
	/**
	 * Check station.
	 *
	 * @param ow the ow
	 * @param idStation the id station
	 * @return true, if successful
	 */
	public boolean checkStation(TicketOnewayDTO ow,char idStation);
	
	/**
	 * Exit by ticket oneway.
	 *
	 * @param ow the ow
	 * @param idStation the id station
	 */
	public void exitByTicketOneway(TicketOnewayDTO ow, char idStation);
	
	/**
	 * Choose ticket oneway.
	 *
	 * @param ow the ow
	 * @param action the action
	 * @param idStation the id station
	 */
	public void chooseTicketOneway(TicketOnewayDTO ow, char action, char idStation);
}
