/*
 * 
 */
package bussinessLayer;

import java.awt.List;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Random;
import java.util.Scanner;

import dataLayer.*;
import dataObject.*;
import presentationLayer.*;
import hust.soict.se.customexception.InvalidIDException;
import hust.soict.se.gate.Gate;
import hust.soict.se.scanner.CardScanner;

// TODO: Auto-generated Javadoc
/**
 * The Class prepaidCardBUS.
 */
public class PrepaidCardBUS extends CardBUS implements PrepaidCardInterface{
	
	/**
	 * Load list prepaid card.
	 *
	 * @return the array list
	 */
	public ArrayList<PrepaidCardDTO> loadListPrepaidCard() {
		String[] listcard,a;
		ReadInfoText re = new ReadInfoText();
		listcard = re.readFile("card.txt");
		ArrayList<PrepaidCardDTO> allCard = new ArrayList<PrepaidCardDTO>();
		for(int i=0;i<listcard.length;i++) {
			if(listcard[i] != null) {
				a = listcard[i].split("-");
				PrepaidCardDTO card = new PrepaidCardDTO();
				card.setPseudocode(a[0]);
				card.setBalance(Float.parseFloat(a[1]));
				allCard.add(card);
			}
		}
		return allCard;
	}
	
	/**
	 * Format output file.
	 *
	 * @param allCard the all card
	 * @return the string[]
	 */
	public String[] formatOutputFile(ArrayList<PrepaidCardDTO> allCard) {
		String[] textInALine = new String[5];
		int i=0;
		for(PrepaidCardDTO card : allCard) {
			textInALine[i] = card.getPseudocode()+"-"+card.getBalance();
			i++;
		}
		return textInALine;
	}
	
	/**
	 * Check prepaid card status.
	 *
	 * @param card the card
	 * @return true, if successful
	 */
	public boolean checkPrepaidCardStatus(PrepaidCardDTO card) {
		if(card.getStatus()==1) {
			return false;
		}
		return true;
	}
	
	/**
	 * Search prepaid card.
	 *
	 * @param card the card
	 * @return the prepaid card DTO
	 */
	public PrepaidCardDTO searchPrepaidCard(PrepaidCardDTO card) {
		ArrayList<PrepaidCardDTO> allCard = new ArrayList<PrepaidCardDTO>();
		PrepaidCardDAO pr = new PrepaidCardDAO();
		allCard = pr.selectCard();
		for(PrepaidCardDTO card2: allCard) {
			if(card.getPseudocode().equals(card2.getPseudocode())==true){
				card = card2;
				break;
			}
		}
		return card;
	}
	
	/**
	 * Checks if is sets the prepaid card.
	 *
	 * @param cardCodePseudo the card code pseudo
	 * @return true, if is sets the prepaid card
	 */
	public boolean issetPrepaidCard(String cardCodePseudo) {
		ArrayList<PrepaidCardDTO> allCard = new ArrayList<PrepaidCardDTO>();
		PrepaidCardDAO pr = new PrepaidCardDAO();
		allCard = pr.selectCard();
		for(PrepaidCardDTO card: allCard) {
			if(cardCodePseudo.equals(card.getPseudocode())==true){
				return true;
			}
		}
		return false;
	}
	
	/**
	 * Enter by prepaid card.
	 *
	 * @param card the card
	 */
	public void enterByPrepaidCard(PrepaidCardDTO card) {
		if(issetPrepaidCard(card.getPseudocode())==true){
			PrepaidCardDAO pr = new PrepaidCardDAO();
    		card = searchPrepaidCard(card);
    		if(checkPrepaidCardStatus(card) == true) {
    			pr.updateStatus(card);
    			TripInfoDAO tr = new TripInfoDAO();
        		tr.updateCode(card.getPseudocode());
        		controllOpenGate();
    			DisplaySuccess dis = new DisplaySuccess();
    			dis.displaySuccess(card);
    		}
    		else {
    			String error = "This card is using by another person.";
    			displayPrepaidCardError(card,error);
    		}
    	}
    	else {
    		card.setId(createIdCard());
    		PrepaidCardDAO pr = new PrepaidCardDAO();
    		pr.insertCard(card);
    		TripInfoDAO tr = new TripInfoDAO();
    		tr.updateCode(card.getPseudocode());
    		controllOpenGate();
    		DisplaySuccess dis = new DisplaySuccess();
			dis.displaySuccess(card);
    	}
	}
	
	/**
	 * Exit by pre paid card.
	 *
	 * @param card the card
	 * @param idStation the id station
	 */
	public void exitByPrePaidCard(PrepaidCardDTO card, char idStation) {
		card = searchPrepaidCard(card);
    	if(checkPrepaidCardStatus(card)==true){
    		String error = "Card not entered.";
    		displayPrepaidCardError(card,error);
    	}
    	else {
    		char idStartStation;
    		char idEndStation = idStation;
    		TripInfoDAO tr = new TripInfoDAO();
    		idStartStation = tr.searchStartStation(card, idEndStation);
    		CalculateBUS C = new CalculateBUS();
    		float distance = C.distance(idStartStation, idEndStation);
    		float price = C.pricing(distance);
    		if(C.checkBalance(card.getBalance(), price)==true) {
    			card.setBalance(card.getBalance()-price);
    			ArrayList<PrepaidCardDTO> allCard = loadListPrepaidCard();
    			int i=0;
    			for(PrepaidCardDTO card2 : allCard) {
    				if(card2.getPseudocode().equals(card.getPseudocode())) {
    					allCard.get(i).setBalance(card.getBalance());
    				}
    				i++;
    			}
    			String[] textInAline = formatOutputFile(allCard);
    			ReadInfoText re = new ReadInfoText();
    			re.editFile(textInAline,"card.txt");
    			PrepaidCardDAO pr = new PrepaidCardDAO();
    			pr.updateCard(card);
    			controllOpenGate();
    			DisplaySuccess dis = new DisplaySuccess();
    			dis.displaySuccess(card);
    		}
    		else {
    			String error = "Not enough balance: ";
    			displayPrepaidcardError(card,error,price);
    		}
    	}
	}
	
	/**
	 * Display prepaid card error.
	 *
	 * @param card the card
	 * @param error the error
	 */
	public void displayPrepaidCardError(PrepaidCardDTO card, String error) {
		presentationLayer.DisplayError dis = new presentationLayer.DisplayError();
		dis.displayError(card, error);
	}
	
	/**
	 * Display prepaidcard error.
	 *
	 * @param card the card
	 * @param error the error
	 * @param price the price
	 */
	public void displayPrepaidcardError(PrepaidCardDTO card, String error, float price) {
		presentationLayer.DisplayError dis = new presentationLayer.DisplayError();
		dis.displayError(card, error,price);
	}
	
	/**
	 * Choose prepaid card.
	 *
	 * @param card the card
	 * @param action the action
	 * @param idStation the id station
	 */
	public void choosePrepaidCard(PrepaidCardDTO card, char action, char idStation) {
		card.setCorrescode(createCardCorresCode(card.getPseudocode()));;
        if(Character.toString(action).equals("1")) {
        	enterByPrepaidCard(card);
        }
        else {
        	exitByPrePaidCard(card, idStation);
        }
	}
}

