/*
 *System Name: AFCsystem 
 */
package bussinessLayer;

import dataLayer.StationDAO;

// TODO: Auto-generated Javadoc
/**
 * The Class calculateBUS.
 */
public class CalculateBUS implements CalculateInterface {
	
	/**
	 * Gets the price min.
	 *
	 * @return the price min
	 */
	@Override
	public float getPriceMin() {
		return (float) 1.9;
	}
	
	/**
	 * Gets the landmark distance.
	 *
	 * @return the landmark distance
	 */
	@Override
	public float getLandmarkDistance() {
		return (float) 5;
	}
	
	/**
	 * Gets the price jump.
	 *
	 * @return the price jump
	 */
	@Override
	public float getPriceJump() {
		return (float) 0.4;
	}
	
	/**
	 * Gets the distance jump.
	 *
	 * @return the distance jump
	 */
	@Override
	public float getDistanceJump() {
		return (float) 2;
	}
	
	/**
	 * Distance.
	 *
	 * @param startStation the start station
	 * @param endStation the end station
	 * @return the float
	 */
	public float distance(char startStation, char endStation) {
		float distanceStart;
		float distanceEnd;
		float distance;
		StationDAO st = new StationDAO();
		distanceStart = st.distance(startStation);
		distanceEnd = st.distance(endStation);
		distance = Math.abs(distanceEnd-distanceStart);
		return distance;
	}
	
	/**
	 * Pricing.
	 *
	 * @param distance the distance
	 * @return the float
	 */
	public float pricing(float distance) {
		float price = 0;
		if(distance <= getLandmarkDistance()) {
			price = getPriceMin();
		}
		else {
			if((distance-getLandmarkDistance())%getDistanceJump()==0) {
				price = getPriceMin() + (distance-getLandmarkDistance())*getPriceJump()/2;
			}
			else {
				int a = (int) Math.ceil((distance-getLandmarkDistance())/2);
				price = getPriceMin() + a*getPriceJump();
			}
		}
		price = (float) (Math.round(price*100))/100;
		return price;
	}
	
	/**
	 * Check balance.
	 *
	 * @param balance the balance
	 * @param price the price
	 * @return true, if successful
	 */
	public boolean checkBalance(float balance,float price) {
		if(balance>=price) {
			return true;
		}
		else {
			return false;
		}
	}
}
