/*
 * 
 */
package bussinessLayer;

import java.util.ArrayList;

import dataObject.PrepaidCardDTO;

// TODO: Auto-generated Javadoc
/**
 * The Interface prepaidCardInterface.
 */
public interface PrepaidCardInterface {
	
	/**
	 * Check prepaid card status.
	 *
	 * @param card the card
	 * @return true, if successful
	 */
	public boolean checkPrepaidCardStatus(PrepaidCardDTO card);
	
	/**
	 * Load list prepaid card.
	 *
	 * @return the array list
	 */
	public ArrayList<PrepaidCardDTO> loadListPrepaidCard();
	
	/**
	 * Format output file.
	 *
	 * @param allCard the all card
	 * @return the string[]
	 */
	public String[] formatOutputFile(ArrayList<PrepaidCardDTO> allCard);
	
	/**
	 * Checks if is sets the prepaid card.
	 *
	 * @param cardCodePseudo the card code pseudo
	 * @return true, if is sets the prepaid card
	 */
	public boolean issetPrepaidCard(String cardCodePseudo);
	
	/**
	 * Search prepaid card.
	 *
	 * @param card the card
	 * @return the prepaid card DTO
	 */
	public PrepaidCardDTO searchPrepaidCard(PrepaidCardDTO card);
	
	/**
	 * Enter by prepaid card.
	 *
	 * @param card the card
	 */
	public void enterByPrepaidCard(PrepaidCardDTO card);
	
	/**
	 * Exit by pre paid card.
	 *
	 * @param card the card
	 * @param idStation the id station
	 */
	public void exitByPrePaidCard(PrepaidCardDTO card, char idStation);
	
	/**
	 * Display prepaid card error.
	 *
	 * @param card the card
	 * @param error the error
	 */
	public void displayPrepaidCardError(PrepaidCardDTO card, String error);
}
