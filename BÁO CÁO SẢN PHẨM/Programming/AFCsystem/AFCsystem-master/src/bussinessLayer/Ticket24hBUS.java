/*
 * 
 */
package bussinessLayer;

import java.io.FileNotFoundException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import dataLayer.Ticket24hDAO;
import dataLayer.ReadInfoText;
import dataLayer.TripInfoDAO;
import dataObject.Ticket24hDTO;
import hust.soict.se.customexception.InvalidIDException;
import hust.soict.se.recognizer.TicketRecognizer;
import presentationLayer.DisplayError;
import presentationLayer.DisplaySuccess;

// TODO: Auto-generated Javadoc
/**
 * The Class ticket24hBUS.
 */
public class Ticket24hBUS extends TicketBUS implements Ticket24hInterface {
	
	/**
	 * Load list ticket 24 h.
	 *
	 * @return the array list
	 */
	public ArrayList<Ticket24hDTO> loadListTicket24h(){
		ReadInfoText re = new ReadInfoText();
		String[] listTicket24h;
		String[] a;
		listTicket24h = re.readFile("twentyfour.txt");
		ArrayList<Ticket24hDTO> allTicket24h = new ArrayList<Ticket24hDTO>();
		for(int i=0;i<listTicket24h.length;i++) {
			if(listTicket24h[i] != null) {
				a = listTicket24h[i].split("-");
				Ticket24hDTO tf = new Ticket24hDTO();
				tf.setPseudocode(a[0]);;
				tf.setStatus(a[1]);
				if(a.length==3) {
					tf.setEnd(a[2]);
				}
				allTicket24h .add(tf);
			}
		}
		return allTicket24h;
	}
	
	/**
	 * Format date.
	 *
	 * @return the string
	 */
	public String formatDate() {
		Calendar c = Calendar.getInstance();
		c.roll(Calendar.DAY_OF_YEAR, true);
		SimpleDateFormat dateFormat = new SimpleDateFormat("HH:mm dd/MM/yyyy");
		String tfEnd = dateFormat.format(c.getTime());
		return tfEnd;	
	}
	
	/**
	 * Format output file.
	 *
	 * @param allTicket24h the all ticket 24 h
	 * @return the string[]
	 */
	public String[] formatOutputFile(ArrayList<Ticket24hDTO> allTicket24h) {
		String[] textInALine = new String[5];
		int i=0;
		for(Ticket24hDTO tf : allTicket24h) {
			if(tf.getStatus().equals("New")==true) {
				textInALine[i] = tf.getPseudocode()+"-"+tf.getStatus();
			}
			else {
				textInALine[i] = tf.getPseudocode()+"-"+tf.getStatus()+"-"+tf.getEnd();
			}
			i++;
		}
		return textInALine;
	}
	
	/**
	 * Enter by ticket 24 h new.
	 *
	 * @param tf the tf
	 */
	public void enterByTicket24hNew(Ticket24hDTO tf) {
		int i=0;
		tf.setStatus("Valid until");
		tf.setEnd(formatDate());
		ArrayList<Ticket24hDTO> allTicket24h = loadListTicket24h();
		for(Ticket24hDTO tf2 : allTicket24h) {
			if(tf2.getPseudocode().equals(tf.getPseudocode())==true) {
				allTicket24h.get(i).setStatus(tf.getStatus());
				allTicket24h.get(i).setEnd(tf.getEnd());
			}
			i++;
		}
		formatOutputFile(allTicket24h);
		String[] textInAline = formatOutputFile(allTicket24h);
		ReadInfoText re = new ReadInfoText();
		re.editFile(textInAline,"twentyfour.txt");
		controllOpenGate();
		Ticket24hDAO ti = new Ticket24hDAO();
		ti.insertTicket24h(tf);
		DisplaySuccess dis = new DisplaySuccess();
		dis.displaySuccess(tf);;
	}
	
	/**
	 * Check valid until ticket 24 h.
	 *
	 * @param tf the tf
	 * @return true, if successful
	 */
	public boolean checkValidUntilTicket24h(Ticket24hDTO tf){
		Date dateCurrent = new Date();
		Date date = new Date();
		try {
			SimpleDateFormat datetime = new SimpleDateFormat("HH:mm dd/MM/yyyy");
			date = datetime.parse(tf.getEnd());
			DateTimeFormatter dtf = DateTimeFormatter.ofPattern("HH:mm dd/MM/yyyy");
			LocalDateTime now = LocalDateTime.now();
			dateCurrent = datetime.parse(dtf.format(now));
			date.compareTo(dateCurrent);
		}
		catch (Exception e) {
			
		}
		if(date.compareTo(dateCurrent)==1) {
			return true;
		}
		else {
			return false;
		}
	}
	
	/**
	 * Check ticket 24 h using.
	 *
	 * @param tf the tf
	 * @return true, if successful
	 */
	public boolean checkTicket24hUsing(Ticket24hDTO tf) {
		TripInfoDAO tr = new TripInfoDAO();
		char idEndStation = tr.searchEndStation(tf);
		if(idEndStation != 0) {
			return true;
		}
		else {
			return false;
		}
	}
	
	/**
	 * Choose ticket 24 h.
	 *
	 * @param tf the tf
	 * @param action the action
	 * @param idStation the id station
	 */
	public void chooseTicket24h(Ticket24hDTO tf, char action, char idStation) {
		tf.setId(createIdTicket());
		tf.setCorrescode(createTicketCorresCode(tf.getPseudocode()));
		if(Character.toString(action).equals("1")) {
			if(tf.getStatus().equals("New")) {
				TripInfoDAO tr = new TripInfoDAO();
        		tr.updateCode(tf.getPseudocode());
				enterByTicket24hNew(tf);
			}
			else {
				if(checkValidUntilTicket24h(tf)) {
					if(checkTicket24hUsing(tf)) {
						TripInfoDAO tr = new TripInfoDAO();
		        		tr.updateCode(tf.getPseudocode());
		        		controllOpenGate();
						DisplaySuccess dis = new DisplaySuccess();
						dis.displaySuccess(tf);
					}
					else {
						DisplayError dis = new DisplayError();
						dis.displayError(tf, "This ticket is using by another person.");
					}
				}
				else {
					Calendar c = Calendar.getInstance();
					SimpleDateFormat dateFormat = new SimpleDateFormat("HH:mm dd/MM/yyyy");
					String dateCurrent = dateFormat.format(c.getTime());
					DisplayError dis = new DisplayError();
					dis.displayError(tf, "Expired: Try to enter",dateCurrent);
				}
			}
		}
		else {
			TripInfoDAO tr = new TripInfoDAO();
    		tr.updateEndStation(tf, idStation);
    		controllOpenGate();
			DisplaySuccess dis = new DisplaySuccess();
			dis.displaySuccess(tf);
		}
	}
}
