/*
 * 
 */
package bussinessLayer;

// TODO: Auto-generated Javadoc
/**
 * The Interface calculateInterface.
 */
public interface CalculateInterface {
	
	/**
	 * Gets the price min.
	 *
	 * @return the price min
	 */
	public float getPriceMin();
	
	/**
	 * Gets the landmark distance.
	 *
	 * @return the landmark distance
	 */
	public float getLandmarkDistance();
	
	/**
	 * Gets the price jump.
	 *
	 * @return the price jump
	 */
	public float getPriceJump();
	
	/**
	 * Gets the distance jump.
	 *
	 * @return the distance jump
	 */
	public float getDistanceJump();
	
	/**
	 * Distance.
	 *
	 * @param startStation the start station
	 * @param endStation the end station
	 * @return the float
	 */
	public float distance(char startStation, char endStation);
	
	/**
	 * Pricing.
	 *
	 * @param distance the distance
	 * @return the float
	 */
	public float pricing(float distance);
	
	/**
	 * Check balance.
	 *
	 * @param balance the balance
	 * @param price the price
	 * @return true, if successful
	 */
	public boolean checkBalance(float balance,float price);
}
