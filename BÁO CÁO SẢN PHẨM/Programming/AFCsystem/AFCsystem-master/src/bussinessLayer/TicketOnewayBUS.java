/*
 * 
 */
package bussinessLayer;

import java.util.ArrayList;

import dataLayer.ReadInfoText;
import dataLayer.TicketOnewayDAO;
import dataLayer.TripInfoDAO;
import dataObject.TicketOnewayDTO;
import presentationLayer.DisplayError;
import presentationLayer.DisplaySuccess;

// TODO: Auto-generated Javadoc
/**
 * The Class ticketOnewayBUS.
 */
public class TicketOnewayBUS extends TicketBUS implements TicketOnewayInterface{
	
	/**
	 * Load list ticket oneway.
	 *
	 * @return the array list
	 */
	public ArrayList<TicketOnewayDTO> loadListTicketOneway(){
		ReadInfoText re = new ReadInfoText();
		String[] listTicketOneway;
		String[] a;
		listTicketOneway = re.readFile("oneway.txt");
		ArrayList<TicketOnewayDTO> allTicketOneway = new ArrayList<TicketOnewayDTO>();
		for(int i=0;i<listTicketOneway.length;i++) {
			if(listTicketOneway[i] != null) {
				a = listTicketOneway[i].split(":");
				TicketOnewayDTO ow = new TicketOnewayDTO();
				ow.setPseudocode(a[0]);
				ow.setNameStart(a[1]);
				ow.setNameEnd(a[2]);
				ow.setStatus(a[3]);
				ow.setBalance(Float.parseFloat(a[4]));
				allTicketOneway .add(ow);
			}
		}
		return allTicketOneway;
	}
	
	/**
	 * Format output file.
	 *
	 * @param allTicketOneway the all ticket oneway
	 * @return the string[]
	 */
	public String[] formatOutputFile(ArrayList<TicketOnewayDTO> allTicketOneway) {
		String[] textInALine = new String[5];
		int i=0;
		for(TicketOnewayDTO ow : allTicketOneway) {
			textInALine[i] = ow.getPseudocode()+":"+ow.getNameStart()+":"+ow.getNameEnd()
			+":"+ow.getStatus()+":"+ow.getBalance();
			i++;
		}
		return textInALine;
	}
	
	/**
	 * Search id station.
	 *
	 * @param ow the ow
	 * @return the ticket oneway DTO
	 */
	public TicketOnewayDTO searchIdStation(TicketOnewayDTO ow) {
		TicketOnewayDAO one = new TicketOnewayDAO();
		ow.setIdStart(one.searchIdStation(ow.getNameStart()));
		ow.setIdEnd(one.searchIdStation(ow.getNameEnd()));
		return ow;
	}
	
	/**
	 * Enter by ticket oneway.
	 *
	 * @param ow the ow
	 */
	public void enterByTicketOneway(TicketOnewayDTO ow) {
		int i=0;
		ow.setStatus("In station");
		TicketOnewayDAO ti = new TicketOnewayDAO();
		ti.insertOnewayTicket(ow);
		ArrayList<TicketOnewayDTO> allTicketOneway = new ArrayList<TicketOnewayDTO>();
		allTicketOneway = loadListTicketOneway();
		for(TicketOnewayDTO ow2 : allTicketOneway) {
			if(ow2.getPseudocode().equals(ow.getPseudocode())) {
				allTicketOneway.get(i).setStatus(ow.getStatus());
			}
			i++;
		}
		String[] textInAline = formatOutputFile(allTicketOneway);
		ReadInfoText re = new ReadInfoText();
		re.editFile(textInAline,"oneway.txt");
		controllOpenGate();
		DisplaySuccess dis = new DisplaySuccess();
		dis.displaySuccess(ow);
	}
	
	/**
	 * Check station.
	 *
	 * @param ow the ow
	 * @param idStation the id station
	 * @return true, if successful
	 */
	public boolean checkStation(TicketOnewayDTO ow,char idStation) {
		if((int)ow.getIdEnd()>=(int)idStation&&(int)ow.getIdStart()<=(int)idStation) {
			return true;
		}
		else if((int)ow.getIdEnd()<=(int)idStation&&(int)ow.getIdStart()>=(int)idStation) {
			return true;
		}
		else {
			return false;
		}
	}
	
	/**
	 * Exit by ticket oneway.
	 *
	 * @param ow the ow
	 * @param idStation the id station
	 */
	public void exitByTicketOneway(TicketOnewayDTO ow, char idStation) {
		char idStartStation;
		char idEndStation = idStation;
		TripInfoDAO tr = new TripInfoDAO();
		idStartStation = tr.searchStartStation(ow, idEndStation);
		CalculateBUS C = new CalculateBUS();
		float distance = C.distance(idStartStation, idEndStation);
		float price = C.pricing(distance);
		if(C.checkBalance(ow.getBalance(), price)==true) {
			int i=0;
			ow.setStatus("Destroyed");
			ow.setBalance(ow.getBalance()-price);
			TicketOnewayDAO ti = new TicketOnewayDAO();
			ti.updateBalance(ow);
			ArrayList<TicketOnewayDTO> allTicketOneway = new ArrayList<TicketOnewayDTO>();
			allTicketOneway = loadListTicketOneway();
			for(TicketOnewayDTO ow2 : allTicketOneway) {
				if(ow2.getPseudocode().equals(ow.getPseudocode())) {
					allTicketOneway.get(i).setStatus(ow.getStatus());
					allTicketOneway.get(i).setBalance(ow.getBalance());
				}
				i++;
			}
			String[] textInAline = formatOutputFile(allTicketOneway);
			ReadInfoText re = new ReadInfoText();
			re.editFile(textInAline,"oneway.txt");
			controllOpenGate();
			DisplaySuccess dis = new DisplaySuccess();
			dis.displaySuccess(ow);
		}
		else {
			DisplayError dis = new DisplayError();
			dis.displayError(ow,"Not enough balance:",price);
		}
	}
	
	/**
	 * Choose ticket oneway.
	 *
	 * @param ow the ow
	 * @param action the action
	 * @param idStation the id station
	 */
	public void chooseTicketOneway(TicketOnewayDTO ow, char action, char idStation) {
		ow.setId(createIdTicket());
		ow.setCorrescode(createTicketCorresCode(ow.getPseudocode()));
		if(Character.toString(action).equals("1")) {
			if(ow.getStatus().equals("New")) {
				ow = searchIdStation(ow);
				if(checkStation(ow, idStation)) {
					TripInfoDAO tr = new TripInfoDAO();
	        		tr.updateCode(ow.getPseudocode());
					enterByTicketOneway(ow);
				}
				else {
					DisplayError dis = new DisplayError();
					dis.displayError(ow,"Tickets cannot be used at this station.");
				}
			}
			else if(ow.getStatus().equals("In station")) {
				DisplayError dis = new DisplayError();
				dis.displayError(ow,"This ticket is using by another person.");
			}
			else {
				DisplayError dis = new DisplayError();
				dis.displayError(ow,"Tickets have been used.");
			}
		}
		else {
			if(ow.getStatus().equals("New")) {
				DisplayError dis = new DisplayError();
				dis.displayError(ow,"Card not entered.");
			}
			else if(ow.getStatus().equals("In station")) {
				exitByTicketOneway(ow, idStation);
			}
			else {
				DisplayError dis = new DisplayError();
				dis.displayError(ow,"Tickets have been used.");
			}
		}
	}
}
