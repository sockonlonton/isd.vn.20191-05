/*
 * 
 */
package bussinessLayer;
import hust.soict.se.gate.Gate;

// TODO: Auto-generated Javadoc
/**
 * The Class gateBUS.
 */
public class GateBUS implements GateInterface {
	
	/**
	 * Open close gate.
	 */
	@Override
	public void openCloseGate() {
		Gate gate = Gate.getInstance();
	    gate.open();
	}
}
