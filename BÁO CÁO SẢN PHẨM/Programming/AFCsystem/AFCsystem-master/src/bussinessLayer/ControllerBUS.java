/*
 * 
 */
package bussinessLayer;

import java.sql.SQLException;
import java.util.ArrayList;

import dataLayer.*;
import dataObject.*;
import presentationLayer.*;

// TODO: Auto-generated Javadoc
/**
 * The Class ControllerBUS.
 */
public class ControllerBUS {
	
	/**
	 * Check input.
	 *
	 * @param choice the choice
	 * @return true, if successful
	 */
	public boolean checkInput(String choice) {
		String[] a;
		if(choice.length() != 3) {
			return false;
		}
		else {
			a = choice.split("-");
			if((a[0].equals("1") == false) && (a[0].equals("2") == false)){
				return false;
			}
			else {
				if((int)a[1].charAt(0) <97 || (int)a[1].charAt(0)>105) {
					return false;
				}
				else {
					return true;
				}
			}
		}
	}
	
	/**
	 * Gets the data input.
	 *
	 * @param choice the choice
	 * @throws ClassNotFoundException the class not found exception
	 * @throws SQLException the SQL exception
	 */
	public void GetDataInput(String choice) throws ClassNotFoundException, SQLException {
		if(checkInput(choice)) {
			String[] a;
			char idStation;
			char action;
			a=choice.split("-");
			action = a[0].charAt(0);
			idStation = a[1].charAt(0);
			TripInfoDAO t = new TripInfoDAO();
			if(Character.toString(action).equals("1")) {
				t.insertStartStation(idStation);
			}
			ArrayList<PrepaidCardDTO> allCard = new ArrayList<PrepaidCardDTO>();
			PrepaidCardBUS pr = new PrepaidCardBUS();
			allCard = pr.loadListPrepaidCard();
			ArrayList<Ticket24hDTO> allTicket24h = new ArrayList<Ticket24hDTO>();
			Ticket24hBUS ti = new Ticket24hBUS();
			allTicket24h = ti.loadListTicket24h();
			ArrayList<TicketOnewayDTO> allTicketOneWay = new ArrayList<TicketOnewayDTO>();
			TicketOnewayBUS ow = new TicketOnewayBUS();
			allTicketOneWay = ow.loadListTicketOneway();
			DisplayMenu dis = new DisplayMenu();
			dis.ChoosingType(action,allCard,allTicket24h,allTicketOneWay,idStation);
		}
		else {
			System.out.println("\nThere is no option.\n");
		}
	}
	
	/**
	 * Gets the input code.
	 *
	 * @param action the action
	 * @param allCard the all card
	 * @param allTicket24h the all ticket 24 h
	 * @param allTicketOneWay the all ticket one way
	 * @param pseudoBarCode the pseudo bar code
	 * @param idStation the id station
	 * @return the input code
	 */
	public void getInputCode(char action, ArrayList<PrepaidCardDTO> allCard, ArrayList<Ticket24hDTO> allTicket24h, 
		ArrayList<TicketOnewayDTO> allTicketOneWay, String pseudoBarCode, char idStation) {
		int check = 0;
		PrepaidCardDTO card = new PrepaidCardDTO();
		for(PrepaidCardDTO card2 : allCard) {
			if(card2.getPseudocode().equals(pseudoBarCode)==true) {
				card = card2;
				PrepaidCardBUS pr = new PrepaidCardBUS();
				pr.choosePrepaidCard(card, action,idStation);
				check = 1;
			}
		}
		Ticket24hDTO tf = new Ticket24hDTO();
		for(Ticket24hDTO tf2 : allTicket24h) {
			if(tf2.getPseudocode().equals(pseudoBarCode)==true) {
				tf = tf2;
				Ticket24hBUS ti = new Ticket24hBUS();
				ti.chooseTicket24h(tf, action,idStation);
				check = 1;
			}
		}
		TicketOnewayDTO ow = new TicketOnewayDTO();
		for(TicketOnewayDTO ow2 : allTicketOneWay) {
			if(ow2.getPseudocode().equals(pseudoBarCode)==true) {
				ow = ow2;
				TicketOnewayBUS ti = new TicketOnewayBUS();
				ti.chooseTicketOneway(ow, action, idStation);
				check = 1;
			}
		}
		if(check == 0) {
			System.out.println("\nCode does not exist.\n");
		}
	}
}
