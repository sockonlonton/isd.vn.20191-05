/*
 * 
 */
package dataObject;

// TODO: Auto-generated Javadoc
/**
 * The Class ticketDTO.
 */
public class TicketDTO {
	
	/** The id. */
	private String id;
	
	/** The pseudocode. */
	private String pseudocode;
	
	/** The correscode. */
	private String correscode;
	
	/**
	 * Gets the id.
	 *
	 * @return the id
	 */
	public String getId() {
		return id;
	}
	
	/**
	 * Sets the id.
	 *
	 * @param id the new id
	 */
	public void setId(String id) {
		this.id = id;
	}
	
	/**
	 * Gets the pseudocode.
	 *
	 * @return the pseudocode
	 */
	public String getPseudocode() {
		return pseudocode;
	}
	
	/**
	 * Sets the pseudocode.
	 *
	 * @param pseudocode the new pseudocode
	 */
	public void setPseudocode(String pseudocode) {
		this.pseudocode = pseudocode;
	}
	
	/**
	 * Gets the correscode.
	 *
	 * @return the correscode
	 */
	public String getCorrescode() {
		return correscode;
	}
	
	/**
	 * Sets the correscode.
	 *
	 * @param correscode the new correscode
	 */
	public void setCorrescode(String correscode) {
		this.correscode = correscode;
	}
}
