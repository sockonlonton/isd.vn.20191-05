/*
 * 
 */
package dataObject;

// TODO: Auto-generated Javadoc
/**
 * The Class ticket24hDTO.
 */
public class Ticket24hDTO extends TicketDTO{
	
	/** The status. */
	private String status;
	
	/** The end. */
	private String end;
	
	/**
	 * Gets the status.
	 *
	 * @return the status
	 */
	public String getStatus() {
		return status;
	}
	
	/**
	 * Sets the status.
	 *
	 * @param status the new status
	 */
	public void setStatus(String status) {
		this.status = status;
	}
	
	/**
	 * Gets the end.
	 *
	 * @return the end
	 */
	public String getEnd() {
		return end;
	}
	
	/**
	 * Sets the end.
	 *
	 * @param end the new end
	 */
	public void setEnd(String end) {
		this.end = end;
	}
}
