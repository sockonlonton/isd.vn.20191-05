/*
 * 
 */
package dataObject;

// TODO: Auto-generated Javadoc
/**
 * The Class ticketOnewayDTO.
 */
public class TicketOnewayDTO extends TicketDTO{
	
	/** The balance. */
	private float balance;
	
	/** The status. */
	private String status;
	
	/** The id start. */
	private char idStart;
	
	/** The id end. */
	private char idEnd;
	
	/** The name start. */
	private String nameStart;
	
	/** The name end. */
	private String nameEnd;
	
	/**
	 * Gets the balance.
	 *
	 * @return the balance
	 */
	public float getBalance() {
		return balance;
	}
	
	/**
	 * Sets the balance.
	 *
	 * @param balance the new balance
	 */
	public void setBalance(float balance) {
		this.balance = balance;
	}
	
	/**
	 * Gets the status.
	 *
	 * @return the status
	 */
	public String getStatus() {
		return status;
	}
	
	/**
	 * Sets the status.
	 *
	 * @param status the new status
	 */
	public void setStatus(String status) {
		this.status = status;
	}
	
	/**
	 * Gets the id start.
	 *
	 * @return the id start
	 */
	public char getIdStart() {
		return idStart;
	}
	
	/**
	 * Sets the id start.
	 *
	 * @param idStart the new id start
	 */
	public void setIdStart(char idStart) {
		this.idStart = idStart;
	}
	
	/**
	 * Gets the id end.
	 *
	 * @return the id end
	 */
	public char getIdEnd() {
		return idEnd;
	}
	
	/**
	 * Sets the id end.
	 *
	 * @param idEnd the new id end
	 */
	public void setIdEnd(char idEnd) {
		this.idEnd = idEnd;
	}
	
	/**
	 * Gets the name start.
	 *
	 * @return the name start
	 */
	public String getNameStart() {
		return nameStart;
	}
	
	/**
	 * Sets the name start.
	 *
	 * @param nameStart the new name start
	 */
	public void setNameStart(String nameStart) {
		this.nameStart = nameStart;
	}
	
	/**
	 * Gets the name end.
	 *
	 * @return the name end
	 */
	public String getNameEnd() {
		return nameEnd;
	}
	
	/**
	 * Sets the name end.
	 *
	 * @param nameEnd the new name end
	 */
	public void setNameEnd(String nameEnd) {
		this.nameEnd = nameEnd;
	}
}
