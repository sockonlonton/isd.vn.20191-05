/*
 * 
 */
package dataObject;

// TODO: Auto-generated Javadoc
/**
 * The Class prepaidCardDTO.
 */
public class PrepaidCardDTO extends CardDTO{
	
	/** The balance. */
	private float balance;
	
	/** The status. */
	private int status;
	
	/**
	 * Gets the balance.
	 *
	 * @return the balance
	 */
	public float getBalance() {
		return balance;
	}
	
	/**
	 * Sets the balance.
	 *
	 * @param balance the new balance
	 */
	public void setBalance(float balance) {
		this.balance = balance;
	}
	
	/**
	 * Gets the status.
	 *
	 * @return the status
	 */
	public int getStatus() {
		return status;
	}
	
	/**
	 * Sets the status.
	 *
	 * @param status the new status
	 */
	public void setStatus(int status) {
		this.status = status;
	}
}
