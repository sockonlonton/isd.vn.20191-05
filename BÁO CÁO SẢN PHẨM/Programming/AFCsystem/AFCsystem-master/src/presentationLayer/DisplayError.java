/*
 * 
 */
package presentationLayer;

import java.util.Scanner;

import dataObject.*;

// TODO: Auto-generated Javadoc
/**
 * The Class displayError.
 */
public class DisplayError {
	
	/**
	 * Display error.
	 *
	 * @param card the card
	 * @param error the error
	 */
	public void displayError(PrepaidCardDTO card, String error) {
		System.out.println("Invalid Prepaid Card");
		System.out.println("ID: "+card.getId()+", balance: "+card.getBalance()+" euros");
		System.out.println(error);
		System.out.println("Press any key to continue...");
		Scanner sc = new Scanner(System.in);
		sc.nextLine();
	}
	
	/**
	 * Display error.
	 *
	 * @param card the card
	 * @param error the error
	 * @param price the price
	 */
	public void displayError(PrepaidCardDTO card,String error, float price) {
		System.out.println("Invalid Prepaid Card");
		System.out.println("ID: "+card.getId()+", balance: "+card.getBalance()+" euros");
		System.out.println("Not enough balance: "+price +" euros");
		System.out.println("Press any key to continue...");
		Scanner sc = new Scanner(System.in);
		sc.nextLine();
	}
	
	/**
	 * Display error.
	 *
	 * @param tf the tf
	 * @param error the error
	 */
	public void displayError(Ticket24hDTO tf,String error) {
		System.out.println("Invalid 24h Ticket");
		System.out.println("ID: "+tf.getId()+", valid until "+tf.getEnd());
		System.out.println(error);
		System.out.println("Press any key to continue...");
		Scanner sc = new Scanner(System.in);
		sc.nextLine();
	}
	
	/**
	 * Display error.
	 *
	 * @param tf the tf
	 * @param error the error
	 * @param dateCurrent the date current
	 */
	public void displayError(Ticket24hDTO tf,String error,String dateCurrent) {
		System.out.println("Invalid 24h Ticket");
		System.out.println("ID: "+tf.getId()+", valid until "+tf.getEnd());
		System.out.println(error+" at "+dateCurrent);
		System.out.println("Press any key to continue...");
		Scanner sc = new Scanner(System.in);
		sc.nextLine();
	}
	
	/**
	 * Display error.
	 *
	 * @param ow the ow
	 * @param error the error
	 */
	public void displayError(TicketOnewayDTO ow,String error) {
		System.out.println("Invalid 24h Ticket");
		System.out.println("ID: "+ow.getId()+", balance "+ ow.getBalance()+" euros");
		System.out.println(error);
		System.out.println("Press any key to continue...");
		Scanner sc = new Scanner(System.in);
		sc.nextLine();
	}
	
	/**
	 * Display error.
	 *
	 * @param ow the ow
	 * @param error the error
	 * @param price the price
	 */
	public void displayError(TicketOnewayDTO ow,String error,float price) {
		System.out.println("Invalid 24h Ticket");
		System.out.println("ID: "+ow.getId()+", balance "+ ow.getBalance()+" euros");
		System.out.println("Not enough balance: "+price +" euros");
		System.out.println("Press any key to continue...");
		Scanner sc = new Scanner(System.in);
		sc.nextLine();
	}
}
