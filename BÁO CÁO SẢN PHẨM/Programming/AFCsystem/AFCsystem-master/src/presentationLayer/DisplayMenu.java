/*
 * 
 */
package presentationLayer;

import java.util.ArrayList;
import java.util.Scanner;

import javax.swing.text.html.HTMLDocument.HTMLReader.PreAction;

import bussinessLayer.ControllerBUS;
import bussinessLayer.PrepaidCardBUS;
import bussinessLayer.Ticket24hBUS;
import dataObject.PrepaidCardDTO;
import dataObject.Ticket24hDTO;
import dataObject.TicketOnewayDTO;

// TODO: Auto-generated Javadoc
/**
 * The Class displayMenu.
 */
public class DisplayMenu {
	
	/**
	 * Choosing type.
	 *
	 * @param action the action
	 * @param allCard the all card
	 * @param allTicket24h the all ticket 24 h
	 * @param allTicketOneWay the all ticket one way
	 * @param idStation the id station
	 */
	public void ChoosingType(char action,ArrayList<PrepaidCardDTO> allCard,ArrayList<Ticket24hDTO> allTicket24h, 
			ArrayList<TicketOnewayDTO> allTicketOneWay, char idStation) {
		System.out.println("These are entering tickets/card:");
		for(TicketOnewayDTO ow : allTicketOneWay) {
			System.out.println(ow.getPseudocode()+": One-way ticket between "+ow.getNameStart()+" and "+ow.getNameEnd()
			+": "+ow.getStatus()+" - "+ow.getBalance()+" euros");
		}
		for(Ticket24hDTO tf : allTicket24h) {
			if(tf.getStatus().equals("New")==true) {
				System.out.println(tf.getPseudocode()+": 24h Tickets: "+tf.getStatus());
			}
			else {
				System.out.println(tf.getPseudocode()+": 24h Tickets: "+tf.getStatus()+" "+tf.getEnd());
			}
		}
		for(PrepaidCardDTO card : allCard) {
			System.out.println(card.getPseudocode()+": Prepaid card: "+card.getBalance());;
		}
		System.out.print("Please provide the code you want to enter/exit:");
		Scanner sc = new Scanner(System.in);
		String pseudoBarCode = sc.nextLine();
		ControllerBUS C = new ControllerBUS();
		C.getInputCode(action, allCard,allTicket24h,allTicketOneWay, pseudoBarCode,idStation);
	}
}