/*
 * 
 */
package presentationLayer;

import java.util.Scanner;

import dataLayer.PrepaidCardDAO;
import dataObject.PrepaidCardDTO;
import dataObject.Ticket24hDTO;
import dataObject.TicketOnewayDTO;

// TODO: Auto-generated Javadoc
/**
 * The Class displaySuccess.
 */
public class DisplaySuccess {
	
	/**
	 * Display success.
	 *
	 * @param card the card
	 */
	public void displaySuccess(PrepaidCardDTO card) {
		System.out.println("Type : Card\tID: "+card.getId());
		System.out.println("Balance: "+card.getBalance());
		System.out.println("Press any key to continue...");
		Scanner sc = new Scanner(System.in);
		sc.nextLine();
	}
	
	/**
	 * Display success.
	 *
	 * @param tf the tf
	 */
	public void displaySuccess(Ticket24hDTO tf) {
		System.out.println("Type : 24h Ticket\tID: "+tf.getId());
		System.out.println("Valid until: "+tf.getEnd());
		System.out.println("Press any key to continue...");
		Scanner sc = new Scanner(System.in);
		sc.nextLine();
	}
	
	/**
	 * Display success.
	 *
	 * @param ow the ow
	 */
	public void displaySuccess(TicketOnewayDTO ow) {
		System.out.println("Type : One-way ticket\tID: "+ow.getId());
		System.out.println("Balance: "+ow.getBalance());
		System.out.println("Press any key to continue...");
		Scanner sc = new Scanner(System.in);
		sc.nextLine();
	}
}
