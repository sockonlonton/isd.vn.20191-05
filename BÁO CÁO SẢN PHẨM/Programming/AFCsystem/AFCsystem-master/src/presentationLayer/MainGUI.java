/*
 * 
 */
package presentationLayer;

import java.sql.SQLException;
import java.util.Scanner;

import bussinessLayer.*;

// TODO: Auto-generated Javadoc
/**
 * The Class mainGUI.
 */
public class MainGUI {
	
	/**
	 * Main menu.
	 */
	public static void mainMenu() {
		System.out.println("These are station in the line M14 of Paris");
		System.out.println("a. Saint-Lazare"); 
		System.out.println("b,Madeleine");
		System.out.println("c,Pyramides"); 
		System.out.println("d,Chatelet"); 
		System.out.println("e,Gare de Lyon"); 
		System.out.println("f,Bercy");
		System.out.println("g,Cour Saint-Emilion"); 
		System.out.println("h,Bibliotheque Francois Mitterrand"); 
		System.out.println("i,Olympiades");
		System.out.println("\nAvailable actions: 1. Enter station, 2. Exit station");
		System.out.println("You can provide a combination of number (1 or 2) and a letter from (a to i) to enter or exit a station (using hyphen in between).");
		System.out.print("Enter your choice:");
	}
	
	/**
	 * The main method.
	 *
	 * @param args the arguments
	 * @throws ClassNotFoundException the class not found exception
	 * @throws SQLException the SQL exception
	 */
	public static void main(String[] args) throws ClassNotFoundException, SQLException {
		String choice,pseudoBarCode;
		int con=0;
		do {
			mainMenu();
			Scanner sc = new Scanner(System.in);
		    choice = sc.nextLine();
		    ControllerBUS c = new ControllerBUS();
		    c.GetDataInput(choice);
		}while(con==0);
	}

}
