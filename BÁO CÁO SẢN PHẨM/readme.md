## Báo cáo sản phẩm Nhóm 05 gồm có: 
1. Bản phân tích yêu cầu phần mềm (SRS).
2. Bản thiết kế phần mềm (SDD): Bao gồm:
	+ Thiết kế cấu trúc tĩnh (static structure) bao gồm cả thiết kế CSDL, thiết kế UI.
	+ Thiết kế hành vi động (dynamic structure) của phần mềm.
	+ Phân tích các mức Coupling và Cohesion của bản thiết kế.
	+ Phân tích việc tuân thủ các design principles trong SOLID và áp dụng các mẫu thiết kế như thế nào.
	+ Phân tích các ưu điểm của những việc này đối với những yêu cầu mới cô đã cung cấp.
3. Bản đặc tả chương trình (program spec) và thiết kế kiểm thử đơn vị.
4. Mã nguồn chương trình kèm mã nguồn kiểm thử đơn vị.
5. Video demo chương trình.
6. Phân công công việc các thành viên trong nhóm, đánh giá sự hoàn thành và đóng góp của thành viên trong nhóm.