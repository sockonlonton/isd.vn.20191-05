## The work we have done
- Create class diagram for class:
   - Huyen:
	+ TicketRecognizer's_Interface
	+ TicketController
	+ OneWayTicket
	+ TwentyFourHoursTicket
   - Hung:
	+ CardInterface
	+ CardController
	+ TVM
	+ CardData
   - Hung:
	+ Gate
	+ Gate's_Screen
	+ StationData
