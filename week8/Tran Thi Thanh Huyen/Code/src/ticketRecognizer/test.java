package ticketRecognizer;
import hust.soict.se.customexception.InvalidIDException;
import hust.soict.se.recognizer.TicketRecognizer;

public class test {
	public static void main (String argv[]) {
		String pseudoBarCode = "abcdefgh";
	    TicketRecognizer ticketRecognizer = TicketRecognizer.getInstance();
	    String ticketId;
		try {
			ticketId = ticketRecognizer.process(pseudoBarCode);
		} catch (InvalidIDException e) {
			e.printStackTrace();
		}
	     System.out.println(ticketId);
	}
}
