package dataObject;

public class ticketOneWayDTO {
	private String owId;
	private String owPseudocode;
	private String owCorrescode;
	private float owBalance;
	private String owStatus;
	private char owIdStart;
	private char owIdEnd;
	private String owNameStart;
	private String owNameEnd;
	public String getOwId() {
		return owId;
	}
	public void setOwId(String owId) {
		this.owId = owId;
	}
	public String getOwPseudocode() {
		return owPseudocode;
	}
	public void setOwPseudocode(String owPseudocode) {
		this.owPseudocode = owPseudocode;
	}
	public String getOwCorrescode() {
		return owCorrescode;
	}
	public void setOwCorrescode(String owCorrescode) {
		this.owCorrescode = owCorrescode;
	}
	public float getOwBalance() {
		return owBalance;
	}
	public void setOwBalance(float owBalance) {
		this.owBalance = owBalance;
	}
	public String getOwStatus() {
		return owStatus;
	}
	public void setOwStatus(String owStatus) {
		this.owStatus = owStatus;
	}
	public char getOwIdStart() {
		return owIdStart;
	}
	public void setOwIdStart(char owIdStart) {
		this.owIdStart = owIdStart;
	}
	public char getOwIdEnd() {
		return owIdEnd;
	}
	public void setOwIdEnd(char owIdEnd) {
		this.owIdEnd = owIdEnd;
	}
	public String getOwNameStart() {
		return owNameStart;
	}
	public void setOwNameStart(String owNameStart) {
		this.owNameStart = owNameStart;
	}
	public String getOwNameEnd() {
		return owNameEnd;
	}
	public void setOwNameEnd(String owNameEnd) {
		this.owNameEnd = owNameEnd;
	}
}
