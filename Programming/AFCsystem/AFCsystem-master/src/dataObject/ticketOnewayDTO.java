package dataObject;

public class ticketOnewayDTO extends ticketDTO{
	private float balance;
	private String status;
	private char idStart;
	private char idEnd;
	private String nameStart;
	private String nameEnd;
	public float getBalance() {
		return balance;
	}
	public void setBalance(float balance) {
		this.balance = balance;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public char getIdStart() {
		return idStart;
	}
	public void setIdStart(char idStart) {
		this.idStart = idStart;
	}
	public char getIdEnd() {
		return idEnd;
	}
	public void setIdEnd(char idEnd) {
		this.idEnd = idEnd;
	}
	public String getNameStart() {
		return nameStart;
	}
	public void setNameStart(String nameStart) {
		this.nameStart = nameStart;
	}
	public String getNameEnd() {
		return nameEnd;
	}
	public void setNameEnd(String nameEnd) {
		this.nameEnd = nameEnd;
	}
}
