package dataObject;

public class ticketDTO {
	private String id;
	private String pseudocode;
	private String correscode;
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getPseudocode() {
		return pseudocode;
	}
	public void setPseudocode(String pseudocode) {
		this.pseudocode = pseudocode;
	}
	public String getCorrescode() {
		return correscode;
	}
	public void setCorrescode(String correscode) {
		this.correscode = correscode;
	}
}
