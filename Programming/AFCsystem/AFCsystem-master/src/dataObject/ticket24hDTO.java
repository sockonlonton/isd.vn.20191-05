package dataObject;

public class ticket24hDTO extends ticketDTO{
	private String status;
	private String end;
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getEnd() {
		return end;
	}
	public void setEnd(String end) {
		this.end = end;
	}
}
