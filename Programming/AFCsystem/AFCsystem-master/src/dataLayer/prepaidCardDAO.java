package dataLayer;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;

import dataObject.*;

public class prepaidCardDAO extends connectdb{
    private String query;
    public ArrayList<prepaidCardDTO> selectCard() {
    	ArrayList<prepaidCardDTO> allCard = new ArrayList<prepaidCardDTO>();
	    try {
			Connection conn = getPostgresqlConnection();
			query = "select * from prepaidcard"; 
			PreparedStatement sml = conn.prepareStatement(query);
			ResultSet Rs = sml.executeQuery();
			while(Rs.next()) {
				prepaidCardDTO card = new prepaidCardDTO();
				card.setId(Rs.getString(1));
				card.setPseudocode(Rs.getString(2));
				card.setCorrescode(Rs.getString(3));
				card.setBalance(Rs.getFloat(4));
				card.setStatus(Rs.getInt(5));
				allCard.add(card);
			}
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
	    return allCard;
    }
    public void insertCard(prepaidCardDTO card) {
    	try {
			Connection conn = getPostgresqlConnection();
			Statement st = conn.createStatement();
			query = "INSERT INTO prepaidcard values ('" + card.getId() +"','"+card.getPseudocode()+"','"+card.getCorrescode()+"','"+card.getBalance() + "','1')";
			st.executeUpdate(query);
			
		} catch (Exception e) {
			System.err.println(e.getMessage());
		}
    }
    public void updateCard(prepaidCardDTO card) {
    	try {
			Connection conn = getPostgresqlConnection();
			Statement st = conn.createStatement();
			query = "UPDATE prepaidcard SET cardbalance = " + card.getBalance() + ",cardstatus=0 WHERE cardpseudocode='"+card.getPseudocode()+"'";
			st.executeUpdate(query);
			
		} catch (Exception e) {
			System.err.println(e.getMessage());
		}
    }
    public void updateStatus(prepaidCardDTO card) {
    	try {
			Connection conn = getPostgresqlConnection();
			Statement st = conn.createStatement();
			query = "UPDATE prepaidcard SET cardstatus=1 WHERE cardpseudocode='"+card.getPseudocode()+"'";
			st.executeUpdate(query);
			
		} catch (Exception e) {
			System.err.println(e.getMessage());
		}
    }
}
