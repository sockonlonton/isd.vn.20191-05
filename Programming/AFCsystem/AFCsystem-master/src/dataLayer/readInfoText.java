package dataLayer;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.util.ArrayList;

import dataObject.ticket24hDTO;

public class readInfoText {
	private String directoryCurrent() {
		File file = new File("");
        String currentDirectory = file.getAbsolutePath();
        currentDirectory = currentDirectory+"/info";
		return currentDirectory;
	}
	public String[] readFile(String filename) {
		BufferedReader br = null;		
        String[] textInALine = new String[5];
        try {   
        	int i=0;
        	br = new BufferedReader(new FileReader(directoryCurrent()+"/"+filename));
            while ((textInALine[i] = br.readLine()) != null) {
            	i++;
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                br.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return textInALine;
	}
	public void editFile(String[] textInAline,String filename) {
		try {
			FileWriter writer = new FileWriter(directoryCurrent()+"/"+filename);
			BufferedWriter buffer = new BufferedWriter(writer);
			for(int i=0;i<textInAline.length;i++) {
				if(textInAline[i] != null) {
					buffer.write(textInAline[i]+"\n");
				}
			}
			buffer.close();
		}
		catch (Exception ex) {
			System.out.println("Loi ghi file: "+ex);
		}
	}
}
