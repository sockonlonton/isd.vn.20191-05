package bussinessLayer;

import java.sql.SQLException;
import java.util.ArrayList;

import dataLayer.*;
import dataObject.*;
import presentationLayer.*;

public class controllerBUS {
	public boolean checkInput(String choice) {
		String[] a;
		if(choice.length() != 3) {
			return false;
		}
		else {
			a = choice.split("-");
			if((a[0].equals("1") == false) && (a[0].equals("2") == false)){
				return false;
			}
			else {
				if((int)a[1].charAt(0) <97 || (int)a[1].charAt(0)>105) {
					return false;
				}
				else {
					return true;
				}
			}
		}
	}
	public void GetDataInput(String choice) throws ClassNotFoundException, SQLException {
		if(checkInput(choice)) {
			String[] a;
			char idStation;
			char action;
			a=choice.split("-");
			action = a[0].charAt(0);
			idStation = a[1].charAt(0);
			tripInfoDAO t = new tripInfoDAO();
			if(Character.toString(action).equals("1")) {
				t.insertStartStation(idStation);
			}
			ArrayList<prepaidCardDTO> allCard = new ArrayList<prepaidCardDTO>();
			prepaidCardBUS pr = new prepaidCardBUS();
			allCard = pr.loadListPrepaidCard();
			ArrayList<ticket24hDTO> allTicket24h = new ArrayList<ticket24hDTO>();
			ticket24hBUS ti = new ticket24hBUS();
			allTicket24h = ti.loadListTicket24h();
			ArrayList<ticketOnewayDTO> allTicketOneWay = new ArrayList<ticketOnewayDTO>();
			ticketOnewayBUS ow = new ticketOnewayBUS();
			allTicketOneWay = ow.loadListTicketOneway();
			displayMenu dis = new displayMenu();
			dis.ChoosingType(action,allCard,allTicket24h,allTicketOneWay,idStation);
		}
		else {
			System.out.println("\nThere is no option.\n");
		}
	}
	public void getInputCode(char action, ArrayList<prepaidCardDTO> allCard, ArrayList<ticket24hDTO> allTicket24h, 
		ArrayList<ticketOnewayDTO> allTicketOneWay, String pseudoBarCode, char idStation) {
		int check = 0;
		prepaidCardDTO card = new prepaidCardDTO();
		for(prepaidCardDTO card2 : allCard) {
			if(card2.getPseudocode().equals(pseudoBarCode)==true) {
				card = card2;
				prepaidCardBUS pr = new prepaidCardBUS();
				pr.choosePrepaidCard(card, action,idStation);
				check = 1;
			}
		}
		ticket24hDTO tf = new ticket24hDTO();
		for(ticket24hDTO tf2 : allTicket24h) {
			if(tf2.getPseudocode().equals(pseudoBarCode)==true) {
				tf = tf2;
				ticket24hBUS ti = new ticket24hBUS();
				ti.chooseTicket24h(tf, action,idStation);
				check = 1;
			}
		}
		ticketOnewayDTO ow = new ticketOnewayDTO();
		for(ticketOnewayDTO ow2 : allTicketOneWay) {
			if(ow2.getPseudocode().equals(pseudoBarCode)==true) {
				ow = ow2;
				ticketOnewayBUS ti = new ticketOnewayBUS();
				ti.chooseTicketOneway(ow, action, idStation);
				check = 1;
			}
		}
		if(check == 0) {
			System.out.println("\nCode does not exist.\n");
		}
	}
}
