package bussinessLayer;

import java.io.FileNotFoundException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import dataLayer.ticket24hDAO;
import dataLayer.readInfoText;
import dataLayer.tripInfoDAO;
import dataObject.ticket24hDTO;
import hust.soict.se.customexception.InvalidIDException;
import hust.soict.se.recognizer.TicketRecognizer;
import presentationLayer.displayError;
import presentationLayer.displaySuccess;

public class ticket24hBUS extends ticketBUS implements ticket24hInterface {
	public ArrayList<ticket24hDTO> loadListTicket24h(){
		readInfoText re = new readInfoText();
		String[] listTicket24h;
		String[] a;
		listTicket24h = re.readFile("twentyfour.txt");
		ArrayList<ticket24hDTO> allTicket24h = new ArrayList<ticket24hDTO>();
		for(int i=0;i<listTicket24h.length;i++) {
			if(listTicket24h[i] != null) {
				a = listTicket24h[i].split("-");
				ticket24hDTO tf = new ticket24hDTO();
				tf.setPseudocode(a[0]);;
				tf.setStatus(a[1]);
				if(a.length==3) {
					tf.setEnd(a[2]);
				}
				allTicket24h .add(tf);
			}
		}
		return allTicket24h;
	}
	public String formatDate() {
		Calendar c = Calendar.getInstance();
		c.roll(Calendar.DAY_OF_YEAR, true);
		SimpleDateFormat dateFormat = new SimpleDateFormat("HH:mm dd/MM/yyyy");
		String tfEnd = dateFormat.format(c.getTime());
		return tfEnd;	
	}
	public String[] formatOutputFile(ArrayList<ticket24hDTO> allTicket24h) {
		String[] textInALine = new String[5];
		int i=0;
		for(ticket24hDTO tf : allTicket24h) {
			if(tf.getStatus().equals("New")==true) {
				textInALine[i] = tf.getPseudocode()+"-"+tf.getStatus();
			}
			else {
				textInALine[i] = tf.getPseudocode()+"-"+tf.getStatus()+"-"+tf.getEnd();
			}
			i++;
		}
		return textInALine;
	}
	public void enterByTicket24hNew(ticket24hDTO tf) {
		int i=0;
		tf.setStatus("Valid until");
		tf.setEnd(formatDate());
		ArrayList<ticket24hDTO> allTicket24h = loadListTicket24h();
		for(ticket24hDTO tf2 : allTicket24h) {
			if(tf2.getPseudocode().equals(tf.getPseudocode())==true) {
				allTicket24h.get(i).setStatus(tf.getStatus());
				allTicket24h.get(i).setEnd(tf.getEnd());
			}
			i++;
		}
		formatOutputFile(allTicket24h);
		String[] textInAline = formatOutputFile(allTicket24h);
		readInfoText re = new readInfoText();
		re.editFile(textInAline,"twentyfour.txt");
		controllOpenGate();
		ticket24hDAO ti = new ticket24hDAO();
		ti.insertTicket24h(tf);
		displaySuccess dis = new displaySuccess();
		dis.displaySuccess(tf);;
	}
	public boolean checkValidUntilTicket24h(ticket24hDTO tf){
		Date dateCurrent = new Date();
		Date date = new Date();
		try {
			SimpleDateFormat datetime = new SimpleDateFormat("HH:mm dd/MM/yyyy");
			date = datetime.parse(tf.getEnd());
			DateTimeFormatter dtf = DateTimeFormatter.ofPattern("HH:mm dd/MM/yyyy");
			LocalDateTime now = LocalDateTime.now();
			dateCurrent = datetime.parse(dtf.format(now));
			date.compareTo(dateCurrent);
		}
		catch (Exception e) {
			
		}
		if(date.compareTo(dateCurrent)==1) {
			return true;
		}
		else {
			return false;
		}
	}
	public boolean checkTicket24hUsing(ticket24hDTO tf) {
		tripInfoDAO tr = new tripInfoDAO();
		char idEndStation = tr.searchEndStation(tf);
		if(idEndStation != 0) {
			return true;
		}
		else {
			return false;
		}
	}
	public void chooseTicket24h(ticket24hDTO tf, char action, char idStation) {
		tf.setId(createIdTicket());
		tf.setCorrescode(createTicketCorresCode(tf.getPseudocode()));
		if(Character.toString(action).equals("1")) {
			if(tf.getStatus().equals("New")) {
				tripInfoDAO tr = new tripInfoDAO();
        		tr.updateCode(tf.getPseudocode());
				enterByTicket24hNew(tf);
			}
			else {
				if(checkValidUntilTicket24h(tf)) {
					if(checkTicket24hUsing(tf)) {
						tripInfoDAO tr = new tripInfoDAO();
		        		tr.updateCode(tf.getPseudocode());
		        		controllOpenGate();
						displaySuccess dis = new displaySuccess();
						dis.displaySuccess(tf);
					}
					else {
						displayError dis = new displayError();
						dis.displayError(tf, "This ticket is using by another person.");
					}
				}
				else {
					Calendar c = Calendar.getInstance();
					SimpleDateFormat dateFormat = new SimpleDateFormat("HH:mm dd/MM/yyyy");
					String dateCurrent = dateFormat.format(c.getTime());
					displayError dis = new displayError();
					dis.displayError(tf, "Expired: Try to enter",dateCurrent);
				}
			}
		}
		else {
			tripInfoDAO tr = new tripInfoDAO();
    		tr.updateEndStation(tf, idStation);
    		controllOpenGate();
			displaySuccess dis = new displaySuccess();
			dis.displaySuccess(tf);
		}
	}
}
