package bussinessLayer;

import java.util.ArrayList;

import dataLayer.readInfoText;
import dataLayer.ticketOnewayDAO;
import dataLayer.tripInfoDAO;
import dataObject.ticketOnewayDTO;
import presentationLayer.displayError;
import presentationLayer.displaySuccess;

public class ticketOnewayBUS extends ticketBUS implements ticketOnewayInterface{
	public ArrayList<ticketOnewayDTO> loadListTicketOneway(){
		readInfoText re = new readInfoText();
		String[] listTicketOneway;
		String[] a;
		listTicketOneway = re.readFile("oneway.txt");
		ArrayList<ticketOnewayDTO> allTicketOneway = new ArrayList<ticketOnewayDTO>();
		for(int i=0;i<listTicketOneway.length;i++) {
			if(listTicketOneway[i] != null) {
				a = listTicketOneway[i].split(":");
				ticketOnewayDTO ow = new ticketOnewayDTO();
				ow.setPseudocode(a[0]);
				ow.setNameStart(a[1]);
				ow.setNameEnd(a[2]);
				ow.setStatus(a[3]);
				ow.setBalance(Float.parseFloat(a[4]));
				allTicketOneway .add(ow);
			}
		}
		return allTicketOneway;
	}
	public String[] formatOutputFile(ArrayList<ticketOnewayDTO> allTicketOneway) {
		String[] textInALine = new String[5];
		int i=0;
		for(ticketOnewayDTO ow : allTicketOneway) {
			textInALine[i] = ow.getPseudocode()+":"+ow.getNameStart()+":"+ow.getNameEnd()
			+":"+ow.getStatus()+":"+ow.getBalance();
			i++;
		}
		return textInALine;
	}
	public ticketOnewayDTO searchIdStation(ticketOnewayDTO ow) {
		ticketOnewayDAO one = new ticketOnewayDAO();
		ow.setIdStart(one.searchIdStation(ow.getNameStart()));
		ow.setIdEnd(one.searchIdStation(ow.getNameEnd()));
		return ow;
	}
	public void enterByTicketOneway(ticketOnewayDTO ow) {
		int i=0;
		ow.setStatus("In station");
		ticketOnewayDAO ti = new ticketOnewayDAO();
		ti.insertOnewayTicket(ow);
		ArrayList<ticketOnewayDTO> allTicketOneway = new ArrayList<ticketOnewayDTO>();
		allTicketOneway = loadListTicketOneway();
		for(ticketOnewayDTO ow2 : allTicketOneway) {
			if(ow2.getPseudocode().equals(ow.getPseudocode())) {
				allTicketOneway.get(i).setStatus(ow.getStatus());
			}
			i++;
		}
		String[] textInAline = formatOutputFile(allTicketOneway);
		readInfoText re = new readInfoText();
		re.editFile(textInAline,"oneway.txt");
		controllOpenGate();
		displaySuccess dis = new displaySuccess();
		dis.displaySuccess(ow);
	}
	public boolean checkStation(ticketOnewayDTO ow,char idStation) {
		if((int)ow.getIdEnd()>=(int)idStation&&(int)ow.getIdStart()<=(int)idStation) {
			return true;
		}
		else if((int)ow.getIdEnd()<=(int)idStation&&(int)ow.getIdStart()>=(int)idStation) {
			return true;
		}
		else {
			return false;
		}
	}
	public void exitByTicketOneway(ticketOnewayDTO ow, char idStation) {
		char idStartStation;
		char idEndStation = idStation;
		tripInfoDAO tr = new tripInfoDAO();
		idStartStation = tr.searchStartStation(ow, idEndStation);
		calculateBUS C = new calculateBUS();
		float distance = C.distance(idStartStation, idEndStation);
		float price = C.pricing(distance);
		if(C.checkBalance(ow.getBalance(), price)==true) {
			int i=0;
			ow.setStatus("Destroyed");
			ow.setBalance(ow.getBalance()-price);
			ticketOnewayDAO ti = new ticketOnewayDAO();
			ti.updateBalance(ow);
			ArrayList<ticketOnewayDTO> allTicketOneway = new ArrayList<ticketOnewayDTO>();
			allTicketOneway = loadListTicketOneway();
			for(ticketOnewayDTO ow2 : allTicketOneway) {
				if(ow2.getPseudocode().equals(ow.getPseudocode())) {
					allTicketOneway.get(i).setStatus(ow.getStatus());
					allTicketOneway.get(i).setBalance(ow.getBalance());
				}
				i++;
			}
			String[] textInAline = formatOutputFile(allTicketOneway);
			readInfoText re = new readInfoText();
			re.editFile(textInAline,"oneway.txt");
			controllOpenGate();
			displaySuccess dis = new displaySuccess();
			dis.displaySuccess(ow);
		}
		else {
			displayError dis = new displayError();
			dis.displayError(ow,"Not enough balance:",price);
		}
	}
	public void chooseTicketOneway(ticketOnewayDTO ow, char action, char idStation) {
		ow.setId(createIdTicket());
		ow.setCorrescode(createTicketCorresCode(ow.getPseudocode()));
		if(Character.toString(action).equals("1")) {
			if(ow.getStatus().equals("New")) {
				ow = searchIdStation(ow);
				if(checkStation(ow, idStation)) {
					tripInfoDAO tr = new tripInfoDAO();
	        		tr.updateCode(ow.getPseudocode());
					enterByTicketOneway(ow);
				}
				else {
					displayError dis = new displayError();
					dis.displayError(ow,"Tickets cannot be used at this station.");
				}
			}
			else if(ow.getStatus().equals("In station")) {
				displayError dis = new displayError();
				dis.displayError(ow,"This ticket is using by another person.");
			}
			else {
				displayError dis = new displayError();
				dis.displayError(ow,"Tickets have been used.");
			}
		}
		else {
			if(ow.getStatus().equals("New")) {
				displayError dis = new displayError();
				dis.displayError(ow,"Card not entered.");
			}
			else if(ow.getStatus().equals("In station")) {
				exitByTicketOneway(ow, idStation);
			}
			else {
				displayError dis = new displayError();
				dis.displayError(ow,"Tickets have been used.");
			}
		}
	}
}
