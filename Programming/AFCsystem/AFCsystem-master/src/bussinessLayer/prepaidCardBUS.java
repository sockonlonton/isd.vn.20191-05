package bussinessLayer;

import java.awt.List;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Random;
import java.util.Scanner;

import dataLayer.*;
import dataObject.*;
import presentationLayer.*;
import hust.soict.se.customexception.InvalidIDException;
import hust.soict.se.gate.Gate;
import hust.soict.se.scanner.CardScanner;

public class prepaidCardBUS extends cardBUS implements prepaidCardInterface{
	public ArrayList<prepaidCardDTO> loadListPrepaidCard() {
		String[] listcard,a;
		readInfoText re = new readInfoText();
		listcard = re.readFile("card.txt");
		ArrayList<prepaidCardDTO> allCard = new ArrayList<prepaidCardDTO>();
		for(int i=0;i<listcard.length;i++) {
			if(listcard[i] != null) {
				a = listcard[i].split("-");
				prepaidCardDTO card = new prepaidCardDTO();
				card.setPseudocode(a[0]);
				card.setBalance(Float.parseFloat(a[1]));
				allCard.add(card);
			}
		}
		return allCard;
	}
	public String[] formatOutputFile(ArrayList<prepaidCardDTO> allCard) {
		String[] textInALine = new String[5];
		int i=0;
		for(prepaidCardDTO card : allCard) {
			textInALine[i] = card.getPseudocode()+"-"+card.getBalance();
			i++;
		}
		return textInALine;
	}
	public boolean checkPrepaidCardStatus(prepaidCardDTO card) {
		if(card.getStatus()==1) {
			return false;
		}
		return true;
	}
	public prepaidCardDTO searchPrepaidCard(prepaidCardDTO card) {
		ArrayList<prepaidCardDTO> allCard = new ArrayList<prepaidCardDTO>();
		prepaidCardDAO pr = new prepaidCardDAO();
		allCard = pr.selectCard();
		for(prepaidCardDTO card2: allCard) {
			if(card.getPseudocode().equals(card2.getPseudocode())==true){
				card = card2;
				break;
			}
		}
		return card;
	}
	public boolean issetPrepaidCard(String cardCodePseudo) {
		ArrayList<prepaidCardDTO> allCard = new ArrayList<prepaidCardDTO>();
		prepaidCardDAO pr = new prepaidCardDAO();
		allCard = pr.selectCard();
		for(prepaidCardDTO card: allCard) {
			if(cardCodePseudo.equals(card.getPseudocode())==true){
				return true;
			}
		}
		return false;
	}
	public void enterByPrepaidCard(prepaidCardDTO card) {
		if(issetPrepaidCard(card.getPseudocode())==true){
			prepaidCardDAO pr = new prepaidCardDAO();
    		card = searchPrepaidCard(card);
    		if(checkPrepaidCardStatus(card) == true) {
    			pr.updateStatus(card);
    			tripInfoDAO tr = new tripInfoDAO();
        		tr.updateCode(card.getPseudocode());
        		controllOpenGate();
    			displaySuccess dis = new displaySuccess();
    			dis.displaySuccess(card);
    		}
    		else {
    			String error = "This card is using by another person.";
    			displayPrepaidCardError(card,error);
    		}
    	}
    	else {
    		card.setId(createIdCard());
    		prepaidCardDAO pr = new prepaidCardDAO();
    		pr.insertCard(card);
    		tripInfoDAO tr = new tripInfoDAO();
    		tr.updateCode(card.getPseudocode());
    		controllOpenGate();
    		displaySuccess dis = new displaySuccess();
			dis.displaySuccess(card);
    	}
	}
	public void exitByPrePaidCard(prepaidCardDTO card, char idStation) {
		card = searchPrepaidCard(card);
    	if(checkPrepaidCardStatus(card)==true){
    		String error = "Card not entered.";
    		displayPrepaidCardError(card,error);
    	}
    	else {
    		char idStartStation;
    		char idEndStation = idStation;
    		tripInfoDAO tr = new tripInfoDAO();
    		idStartStation = tr.searchStartStation(card, idEndStation);
    		calculateBUS C = new calculateBUS();
    		float distance = C.distance(idStartStation, idEndStation);
    		float price = C.pricing(distance);
    		if(C.checkBalance(card.getBalance(), price)==true) {
    			card.setBalance(card.getBalance()-price);
    			ArrayList<prepaidCardDTO> allCard = loadListPrepaidCard();
    			int i=0;
    			for(prepaidCardDTO card2 : allCard) {
    				if(card2.getPseudocode().equals(card.getPseudocode())) {
    					allCard.get(i).setBalance(card.getBalance());
    				}
    				i++;
    			}
    			String[] textInAline = formatOutputFile(allCard);
    			readInfoText re = new readInfoText();
    			re.editFile(textInAline,"card.txt");
    			prepaidCardDAO pr = new prepaidCardDAO();
    			pr.updateCard(card);
    			controllOpenGate();
    			displaySuccess dis = new displaySuccess();
    			dis.displaySuccess(card);
    		}
    		else {
    			String error = "Not enough balance: ";
    			displayPrepaidcardError(card,error,price);
    		}
    	}
	}
	public void displayPrepaidCardError(prepaidCardDTO card, String error) {
		presentationLayer.displayError dis = new presentationLayer.displayError();
		dis.displayError(card, error);
	}
	public void displayPrepaidcardError(prepaidCardDTO card, String error, float price) {
		presentationLayer.displayError dis = new presentationLayer.displayError();
		dis.displayError(card, error,price);
	}
	public void choosePrepaidCard(prepaidCardDTO card, char action, char idStation) {
		card.setCorrescode(createCardCorresCode(card.getPseudocode()));;
        if(Character.toString(action).equals("1")) {
        	enterByPrepaidCard(card);
        }
        else {
        	exitByPrePaidCard(card, idStation);
        }
	}
}

