package bussinessLayer;

import java.util.ArrayList;

import dataObject.ticketOnewayDTO;

public interface ticketOnewayInterface {
	public ArrayList<ticketOnewayDTO> loadListTicketOneway();
	public String[] formatOutputFile(ArrayList<ticketOnewayDTO> allTicketOneway);
	public ticketOnewayDTO searchIdStation(ticketOnewayDTO ow);
	public void enterByTicketOneway(ticketOnewayDTO ow);
	public boolean checkStation(ticketOnewayDTO ow,char idStation);
	public void exitByTicketOneway(ticketOnewayDTO ow, char idStation);
	public void chooseTicketOneway(ticketOnewayDTO ow, char action, char idStation);
}
