package presentationLayer;

import java.util.ArrayList;
import java.util.Scanner;

import javax.swing.text.html.HTMLDocument.HTMLReader.PreAction;

import bussinessLayer.controllerBUS;
import bussinessLayer.prepaidCardBUS;
import bussinessLayer.ticket24hBUS;
import dataObject.prepaidCardDTO;
import dataObject.ticket24hDTO;
import dataObject.ticketOnewayDTO;

public class displayMenu {
	public void ChoosingType(char action,ArrayList<prepaidCardDTO> allCard,ArrayList<ticket24hDTO> allTicket24h, 
			ArrayList<ticketOnewayDTO> allTicketOneWay, char idStation) {
		System.out.println("These are entering tickets/card:");
		for(ticketOnewayDTO ow : allTicketOneWay) {
			System.out.println(ow.getPseudocode()+": One-way ticket between "+ow.getNameStart()+" and "+ow.getNameEnd()
			+": "+ow.getStatus()+" - "+ow.getBalance()+" euros");
		}
		for(ticket24hDTO tf : allTicket24h) {
			if(tf.getStatus().equals("New")==true) {
				System.out.println(tf.getPseudocode()+": 24h Tickets: "+tf.getStatus());
			}
			else {
				System.out.println(tf.getPseudocode()+": 24h Tickets: "+tf.getStatus()+" "+tf.getEnd());
			}
		}
		for(prepaidCardDTO card : allCard) {
			System.out.println(card.getPseudocode()+": Prepaid card: "+card.getBalance());;
		}
		System.out.print("Please provide the code you want to enter/exit:");
		Scanner sc = new Scanner(System.in);
		String pseudoBarCode = sc.nextLine();
		controllerBUS C = new controllerBUS();
		C.getInputCode(action, allCard,allTicket24h,allTicketOneWay, pseudoBarCode,idStation);
	}
}