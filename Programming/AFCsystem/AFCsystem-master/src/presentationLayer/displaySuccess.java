package presentationLayer;

import java.util.Scanner;

import dataLayer.prepaidCardDAO;
import dataObject.prepaidCardDTO;
import dataObject.ticket24hDTO;
import dataObject.ticketOnewayDTO;

public class displaySuccess {
	public void displaySuccess(prepaidCardDTO card) {
		System.out.println("Type : Card\tID: "+card.getId());
		System.out.println("Balance: "+card.getBalance());
		System.out.println("Press any key to continue...");
		Scanner sc = new Scanner(System.in);
		sc.nextLine();
	}
	public void displaySuccess(ticket24hDTO tf) {
		System.out.println("Type : 24h Ticket\tID: "+tf.getId());
		System.out.println("Valid until: "+tf.getEnd());
		System.out.println("Press any key to continue...");
		Scanner sc = new Scanner(System.in);
		sc.nextLine();
	}
	public void displaySuccess(ticketOnewayDTO ow) {
		System.out.println("Type : One-way ticket\tID: "+ow.getId());
		System.out.println("Balance: "+ow.getBalance());
		System.out.println("Press any key to continue...");
		Scanner sc = new Scanner(System.in);
		sc.nextLine();
	}
}
